import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from sklearn.datasets import load_digits
from sklearn.preprocessing import normalize


# Load the digits dataset, normalize, shuffle and one-hot encode labels
digits = load_digits()
all_features = normalize(digits['data'], axis=0).astype(np.float32)
all_labels = np.eye(digits['target_names'].size)[digits['target']].astype(np.float32)
all_features, all_labels = shuffle(all_features, all_labels)
split = round(0.80 * all_features.shape[0])
train_features, train_labels = all_features[:split][:], all_labels[:split][:]
valid_features, valid_labels = all_features[split:][:], all_labels[split:][:]


# Create inputs and labels
features = tf.placeholder(tf.float32, [None, all_features.shape[1]])
labels = tf.placeholder(tf.float32, [None, all_labels.shape[1]])

# Neural network architecture (single layer ~90% accuracy, one hidden layer ~95% accuracy)
hidden_neurons = 20  # 20-40 hidden neurons yield best results
hidden_weights = tf.Variable(tf.truncated_normal((all_features.shape[1], hidden_neurons)))
hidden_biases = tf.Variable(tf.zeros(hidden_neurons))
output_weights = tf.Variable(tf.truncated_normal((hidden_neurons, all_labels.shape[1])))
output_biases = tf.Variable(tf.zeros(all_labels.shape[1]))

# Feed dicts for training and validation
train_dict = {features: train_features, labels: train_labels}
valid_dict = {features: valid_features, labels: valid_labels}

# Linear function WX + b, one hidden sigmoid activated layer and softmax prediction
hidden_layer = tf.nn.sigmoid(tf.matmul(features, hidden_weights) + hidden_biases)
logits = tf.matmul(hidden_layer, output_weights) + output_biases
prediction = tf.nn.softmax(logits)

# Cross entropy, training loss and accuracy
cross_entropy = -tf.reduce_sum(labels * tf.log(prediction), reduction_indices=[1])
loss = tf.reduce_mean(cross_entropy)
correct_prediction = tf.equal(tf.argmax(prediction, 1), tf.argmax(labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Gradient descent optimizer
epochs = 3500
batch_size = 850
learning_rate = 1.0
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)

# Plot storage
plt_epoch, plt_loss, plt_train_accuracy, plt_valid_accuracy = [], [], [], []


# Train the network
with tf.Session() as session:
    session.run(tf.global_variables_initializer())

    # Training cycles
    for epoch in range(epochs):
        for batch, start in enumerate(range(0, train_features.shape[0], batch_size)):
            # Get a batch of training features and labels
            stop = min(start + batch_size, train_features.shape[0])
            batch_features = train_features[start:stop]
            batch_labels = train_labels[start:stop]

            # Run optimizer and get loss
            _, current_loss = session.run([optimizer, loss], feed_dict={features: batch_features, labels: batch_labels})

        # Calculate training and validation accuracy for plotting
        if epoch % 10 == 0:
            train_accuracy = session.run(accuracy, feed_dict=train_dict)
            valid_accuracy = session.run(accuracy, feed_dict=valid_dict)
            plt_epoch.append(epoch)
            plt_loss.append(current_loss)
            plt_train_accuracy.append(train_accuracy)
            plt_valid_accuracy.append(valid_accuracy)
            print("Epoch {}/{}".format(epoch, epochs))

    # Final validation accuracy
    valid_accuracy = session.run(accuracy, feed_dict=valid_dict)
    print("Final accuracy {:.2%}.".format(valid_accuracy))


# Loss and accuracy plots
loss_plot = plt.subplot(211)
loss_plot.set_title('Loss')
loss_plot.plot(plt_epoch, plt_loss, 'g')
loss_plot.set_xlim([plt_epoch[0], plt_epoch[-1]])
acc_plot = plt.subplot(212)
acc_plot.set_title('Accuracy')
acc_plot.plot(plt_epoch, plt_train_accuracy, 'r', label='Training Accuracy')
acc_plot.plot(plt_epoch, plt_valid_accuracy, 'x', label='Validation Accuracy')
acc_plot.set_ylim([0, 1.0])
acc_plot.set_xlim([plt_epoch[0], plt_epoch[-1]])
acc_plot.legend(loc=4)
plt.tight_layout()
plt.show()
