import time
import scipy.misc
import numpy as np
import pandas as pd
import tensorflow as tf

from alexnet import AlexNet

# Load dataset
sign_names = pd.read_csv('data/names.csv', header=None)
nb_classes = len(sign_names)

# Placeholders
x = tf.placeholder(tf.float32, (None, 32, 32, 3))
resized = tf.image.resize_images(x, (227, 227))

# AlexNet
fc7 = AlexNet(resized, feature_extract=True)
# NOTE: By setting `feature_extract` to `True` we return the second to last layer.

# TODO: Define a new fully connected layer followed by a softmax activation to classify
#       the traffic signs. Assign the result of the softmax activation to `probs` below.
shape = (fc7.get_shape().as_list()[-1], nb_classes)  # use this shape for the weight matrix
fc8W = tf.Variable(tf.truncated_normal(shape, stddev=0.1))
fc8b = tf.Variable(tf.zeros(shape[1]))
fc8 = tf.nn.xw_plus_b(fc7, fc8W, fc8b)
probs = tf.nn.softmax(fc8)

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

# Read Images
im1 = scipy.misc.imread('images/construction.jpg').astype(np.float32)
im1 = im1 - np.mean(im1)

im2 = scipy.misc.imread('images/stop.jpg').astype(np.float32)
im2 = im2 - np.mean(im2)

# Run Inference
t = time.time()
output = sess.run(probs, feed_dict={x: [im1, im2]})

# Print Output
for input_im_ind in range(output.shape[0]):
    inds = np.argsort(output)[input_im_ind, :]
    print("Image", input_im_ind)
    for i in range(5):
        print("%s: %.3f" % (sign_names.ix[inds[-1 - i]][1], output[input_im_ind, inds[-1 - i]]))
    print()

print("Time: %.3f seconds" % (time.time() - t))
