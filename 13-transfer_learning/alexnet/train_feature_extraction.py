import tensorflow as tf
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split

from alexnet import AlexNet
from signs import load_dataset, load_names


# TODO: Load traffic signs data.
all_x, all_y = load_dataset('data/train.p')
names_y = load_names('data/names.csv')


# TODO: Split data into training and validation sets.
train_x, test_x, train_y, test_y = train_test_split(all_x, all_y, test_size=0.2)


# TODO: Define placeholders and resize operation.
x = tf.placeholder(tf.float32, (None, 32, 32, 3))
y = tf.placeholder(tf.int32, None)
resized = tf.image.resize_images(x, (227, 227))


# TODO: pass placeholder as first argument to `AlexNet`.
fc7 = AlexNet(resized, feature_extract=True)
# NOTE: `tf.stop_gradient` prevents the gradient from flowing backwards
# past this point, keeping the weights before and up to `fc7` frozen.
# This also makes training faster, less work to do!
fc7 = tf.stop_gradient(fc7)


# TODO: Add the final layer for traffic sign classification.
shape = (fc7.get_shape().as_list()[-1], len(names_y))
fc8W = tf.Variable(tf.truncated_normal(shape, stddev=1e-2))
fc8b = tf.Variable(tf.zeros(len(names_y)))
logits = tf.nn.xw_plus_b(fc7, fc8W, fc8b)
probs = tf.nn.softmax(logits)


# TODO: Define loss, training, accuracy operations.
cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits, tf.one_hot(y, len(names_y)))
loss = tf.reduce_mean(cross_entropy)
prediction = tf.cast(tf.argmax(logits, axis=1), tf.int32)
correct_prediction = tf.equal(prediction, y)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


# TODO: Train and evaluate the feature extraction model.
epochs = 30
batch_size = 512
learning_rate = 0.003

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)
saver = tf.train.Saver()

with tf.Session() as session:
    session.run(tf.global_variables_initializer())

    for epoch in range(epochs):
        for batch, start in enumerate(range(0, train_x.shape[0], batch_size)):
            stop = min(start + batch_size, train_x.shape[0])
            batch_dict = {x: train_x[start:stop], y: train_y[start:stop]}
            _, current_loss = session.run([optimizer, loss], feed_dict=batch_dict)

        test_dict = {x: test_x[:batch_size], y: test_y[:batch_size]}
        batch_dict = {x: train_x[start:stop], y: train_y[start:stop]}
        train_accuracy = session.run(accuracy, feed_dict=batch_dict)
        test_accuracy = session.run(accuracy, feed_dict=test_dict)
        print("Epoch {}/{} | Accuracy {:.2%}/{:.2%}".format(epoch, epochs, train_accuracy,
                                                            test_accuracy))

    # Save the trained model
    saver.save(session, 'alexnet.ckpt')

    # Final validation accuracy and confusion matrix
    test_dict = {x: test_x[:batch_size], y: test_y[:batch_size]}
    test_prediction, test_accuracy = session.run([prediction, accuracy], feed_dict=test_dict)
    confusion_matrix = confusion_matrix(test_dict[y], test_prediction)
    print("Final test accuracy {:.2%}.".format(test_accuracy))
