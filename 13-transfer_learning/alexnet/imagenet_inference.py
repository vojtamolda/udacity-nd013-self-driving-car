import time
import scipy.misc
import numpy as np
import tensorflow as tf

from caffe_classes import class_names
from alexnet import AlexNet


# Placeholder
x = tf.placeholder(tf.float32, (None, 227, 227, 3))

# AlexNet
probs = AlexNet(x)
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

# Read Images
im1 = (scipy.misc.imread('images/poodle.png')[:, :, :3]).astype(np.float32)
im1 = im1 - np.mean(im1)

im2 = (scipy.misc.imread('images/weasel.png')[:, :, :3]).astype(np.float32)
im2 = im2 - np.mean(im2)

# Run Inference
t = time.time()
output = sess.run(probs, feed_dict={x: [im1, im2]})

# Print Output
for input_im_ind in range(output.shape[0]):
    inds = np.argsort(output)[input_im_ind, :]
    print("Image", input_im_ind)
    for i in range(5):
        print("%s: %.3f" % (class_names[inds[-1 - i]], output[input_im_ind, inds[-1 - i]]))
    print()

print("Time: %.3f seconds" % (time.time() - t))
