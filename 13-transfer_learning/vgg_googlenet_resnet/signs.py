import unittest
import numpy as np
import sklearn as skl
import sklearn.metrics
import tensorflow as tf
import matplotlib.pyplot as plt


# Visiualization of the dataset
def visualize_dataset(x, y, names_y):
    # Plot sample from the dataset
    figure = plt.figure()
    for label, name in enumerate(names_y):
        label_idx = np.where(y == label)[0]
        subplot = figure.add_subplot(1, 10, label + 1)
        subplot.set_title(name)
        subplot.axes.get_xaxis().set_visible(False)
        subplot.axes.get_yaxis().set_visible(False)
        images = np.concatenate(x[label_idx[0:10]], axis=0)
        plt.imshow(images.squeeze(), cmap='gray')
    plt.show(block=False)

    # Plot histogram of label frequency
    plt.figure()
    plt.hist(y, bins=len(names_y), orientation='horizontal')
    plt.title("Label Frequencies")
    plt.yticks(range(len(names_y)), names_y)
    plt.show(block=False)


# Preprocessing of the dataset
def preprocess_dataset(x, y):
    # Use Y-luminance (YCbCr) to convert RGB to grayscale
    x = x.astype(np.float32)
    x = 0.299 * x[:, :, :, [0]] + 0.587 * x[:, :, :, [1]] + 0.114 * x[:, :, :, [2]]

    # Locally normalize each image to zero mean and unit variance
    loc_mean = x.mean(axis=(1, 2), keepdims=True)
    loc_range = x.max(axis=(1, 2), keepdims=True) - x.min(axis=(1, 2), keepdims=True)
    x = (x - loc_mean) / loc_range

    return x, y


# Convolutional neural network architecture
def lenet(x, y, drop):
    # Parameters for normally distributed initial weights
    mean, stddev = 0, 0.1

    # Layer 1: Convolutional. Input = 32x32x1. Output = 28x28x24. Filter = 5x5. Activation = ReLU.
    filter1 = tf.Variable(tf.truncated_normal([5, 5, 1, 24], mean=mean, stddev=stddev))
    bias1 =  tf.Variable(tf.zeros([24]))
    layer1 = tf.nn.conv2d(x, filter1, strides=[1, 1, 1, 1], padding='VALID') + bias1
    layer1 = tf.nn.relu(layer1)
    # Pooling. Input = 28x28x24. Output = 14x14x24. Kernel = 2x2.
    pool1 = tf.nn.max_pool(layer1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

    # Layer 2: Convolutional. Input = 14x14x24. Output = 10x10x48. Filter = 5x5. Activation = ReLU.
    filter2 = tf.Variable(tf.truncated_normal([5, 5, 24, 48], mean=mean, stddev=stddev))
    bias2 = tf.Variable(tf.zeros([48]))
    layer2 = tf.nn.conv2d(pool1, filter2, strides=[1, 1, 1, 1], padding='VALID') + bias2
    layer2 = tf.nn.relu(layer2)
    # Pooling. Input = 10x10x48. Output = 5x5x48. Kernel = 2x2.
    layer2 = tf.nn.max_pool(layer2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')
    layer2 = tf.contrib.layers.flatten(layer2)

    # Layer 3: Fully Connected. Input = 1200. Output = 320. Activation = ReLU.
    weight3 = tf.Variable(tf.truncated_normal([1200, 320], mean=mean, stddev=stddev))
    bias3 = tf.Variable(tf.zeros([320]))
    layer3 = tf.matmul(layer2, weight3) + bias3
    layer3 = tf.nn.tanh(layer3)

    # Layer 4: Fully Connected. Input = 320. Output = 120. Activation = ReLU.
    weight4 = tf.Variable(tf.truncated_normal([320, 120], mean=mean, stddev=stddev))
    bias4 = tf.Variable(tf.zeros([120]))
    layer4 = tf.matmul(layer3, weight4) + bias4
    layer4 = tf.nn.dropout(layer4, keep_prob=drop)
    layer4 = tf.nn.tanh(layer4)

    # Layer 5: Fully Connected. Input = 120. Output = 10.
    weight5 = tf.Variable(tf.truncated_normal([120, 10], mean=mean, stddev=stddev))
    bias5 = tf.Variable(tf.zeros([10]))
    layer5 = tf.matmul(layer4, weight5) + bias5
    layer5 = tf.nn.dropout(layer5, keep_prob=drop)
    logits = layer5

    # Metrics
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits, tf.one_hot(y, 10))
    loss = tf.reduce_mean(cross_entropy)
    prediction = tf.cast(tf.argmax(logits, axis=1), tf.int32)
    correct_prediction = tf.equal(prediction, y)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    return logits, loss, prediction, accuracy


# Train the neural network
def train_cnn(train_x, train_y, test_x, test_y, names_y):
    # Hyperparameters
    epochs = 30
    batch_size = 2048
    learning_rate = 0.01

    # Features and labels
    x = tf.placeholder(tf.float32, [None, 32, 32, 1])
    y = tf.placeholder(tf.int32, [None])
    drop = tf.placeholder(tf.float32)

    # Loss, accuracy and optimizer
    logits, loss, prediction, accuracy = lenet(x, y, drop)
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)
    saver = tf.train.Saver()

    # Plot storage
    plt_epoch, plt_loss, plt_train_accuracy, plt_test_accuracy = [], [], [], []

    # Train the network
    with tf.Session() as session:
        session.run(tf.global_variables_initializer())

        # Training cycles
        for epoch in range(epochs):
            for batch, start in enumerate(range(0, train_x.shape[0], batch_size)):
                # Get a batch of training features and labels
                stop = min(start + batch_size, train_x.shape[0])
                batch_dict = {x: train_x[start:stop], y: train_y[start:stop], drop: 0.75}
                # Run optimizer and get loss
                _, current_loss = session.run([optimizer, loss], feed_dict=batch_dict)

            # Calculate training and validation accuracy for plotting
            test_dict = {x: test_x[:batch_size], y: test_y[:batch_size], drop: 1.0}
            batch_dict = {x: train_x[start:stop], y: train_y[start:stop], drop: 1.0}
            train_accuracy = session.run(accuracy, feed_dict=batch_dict)
            test_accuracy = session.run(accuracy, feed_dict=test_dict)
            plt_epoch.append(epoch)
            plt_loss.append(current_loss)
            plt_train_accuracy.append(train_accuracy)
            plt_test_accuracy.append(test_accuracy)
            print("Epoch {}/{} | Accuracy {:.2%}/{:.2%}".format(epoch, epochs, train_accuracy, test_accuracy))

        # Save the trained model
        saver.save(session, 'results/lenet.ckpt')

        # Final validation accuracy and confusion matrix
        test_dict = {x: test_x[:3 * batch_size], y: test_y[:3 * batch_size], drop: 1.0}
        test_prediction, test_accuracy = session.run([prediction, accuracy], feed_dict=test_dict)
        confusion_matrix = skl.metrics.confusion_matrix(test_dict[y], test_prediction)
        print("Final test accuracy {:.2%}.".format(test_accuracy))

    # Loss and accuracy plots
    plt.figure()
    loss_plot = plt.subplot(2, 1, 1)
    loss_plot.set_title('Loss')
    loss_plot.plot(plt_epoch, plt_loss, 'g')
    loss_plot.set_xlim([plt_epoch[0], plt_epoch[-1]])
    acc_plot = plt.subplot(2, 1, 2)
    acc_plot.set_title('Accuracy')
    acc_plot.plot(plt_epoch, plt_train_accuracy, 'r', label='Training Accuracy')
    acc_plot.plot(plt_epoch, plt_test_accuracy, 'x', label='Test Accuracy')
    acc_plot.set_ylim([0, 1.0])
    acc_plot.set_xlim([plt_epoch[0], plt_epoch[-1]])
    acc_plot.legend(loc=4)
    plt.tight_layout()
    plt.show()

    # Relative confusion matrix plot
    plt.figure()
    confusion_matrix = confusion_matrix / confusion_matrix.sum(axis=1)[:, np.newaxis] * 100.0
    plt.imshow(confusion_matrix, interpolation='nearest', vmin=0, vmax=10, cmap='Blues')
    plt.title("Confusion Matrix")
    plt.xticks(range(len(names_y)), names_y, rotation=90)
    plt.yticks(range(len(names_y)), names_y)
    plt.xlabel('Prediction')
    plt.ylabel('Label')
    plt.colorbar()
    plt.show()


def predict_cnn(pred_x, pred_y, names_y):
    # Features and labels
    x = tf.placeholder(tf.float32, [None, 32, 32, 1])
    y = tf.placeholder(tf.int32, [None])
    drop = tf.placeholder(tf.float32)

    # Loss, accuracy and optimizer
    logits, loss, prediction, accuracy = lenet(x, y, drop)
    saver = tf.train.Saver()

    # Load saved network
    with tf.Session() as session:
        saver.restore(session, 'results/lenet.ckpt')
        cnn_logits, cnn_prediction, cnn_accuracy = session.run([logits,  prediction, accuracy],
                                                               feed_dict={x: pred_x, y: pred_y, drop: 1.0})
        print("Accuracy {:.2%}".format(cnn_accuracy))

    # Visualize predictions and activations.
    figure = plt.figure()
    for i, (image, label) in enumerate(zip(pred_x, pred_y)):
        imgaplot = figure.add_subplot(len(pred_y), 1, i + 1)
        imgaplot.set_title("Prediction: " + names_y[cnn_prediction[i]])
        imgaplot.axes.get_xaxis().set_visible(False)
        imgaplot.axes.get_yaxis().set_visible(False)
        plt.imshow(image.squeeze(), cmap='gray')
        probplot = figure.add_subplot(len(pred_y), 2, 2 * (i + 1))
        probplot.set_title("Label: " + names_y[label])
        probplot.set_xlim([-1, 44])
        probplot.set_ylim([0, 1])
        cnn_probs = np.exp(cnn_logits[i]) / np.sum(np.exp(cnn_logits[i]), axis=0)
        plt.bar(range(len(names_y)), cnn_probs, align='center', width=0.9)
    plt.show()


# Tests. Run as python -m unittest signs.py
class SignsClassifier(unittest.TestCase):

    def setUp(self):
        from keras.datasets import cifar10
        (self.train_x, self.train_y), (self.test_x, self.test_y) = cifar10.load_data()
        self.train_x, self.train_y = np.rollaxis(self.train_x, 1, 4), self.train_y.flatten()
        self.test_x, self.test_y = np.rollaxis(self.test_x, 1, 4), self.test_y.flatten()
        self.names_y = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']

        self.assertEqual(len(self.train_x), len(self.train_y))
        self.assertEqual(len(self.test_x), len(self.test_y))

    def test_dataset(self):
        # What's the number of training examples
        n_train = len(self.train_x)
        # What's the number of testing examples?
        n_test = len(self.test_x)
        # What's the shape of an traffic sign image?
        image_shape = self.train_x[0].shape
        # How many unique classes/labels there are in the dataset?
        n_classes = len(self.names_y)
        print("Number of training examples = {}".format(n_train))
        print("Number of testing examples = {}".format(n_test))
        print("Image data shape = {}".format(image_shape))
        print("Number of classes = {}".format(n_classes))

    def test_preprocessing(self):
        visualize_dataset(self.train_x, self.train_y, self.names_y)
        train_x, train_y = preprocess_dataset(self.train_x, self.train_y)
        visualize_dataset(train_x, train_y, self.names_y)

    def test_training(self):
        train_x, train_y = preprocess_dataset(self.train_x, self.train_y)
        test_x, test_y = preprocess_dataset(self.test_x, self.test_y)
        train_cnn(train_x, train_y, test_x, test_y, self.names_y)

    def test_prediction(self):
        test_x, test_y = preprocess_dataset(self.test_x, self.test_y)
        predict_cnn(test_x[:2000], test_y[:2000], self.names_y)
