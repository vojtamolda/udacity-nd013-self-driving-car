import pickle
import numpy as np
import tensorflow as tf
# TODO: Import keras layers you need here
from keras.layers import Input, Flatten, Dense
from keras.models import Model


# Command line flags
flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_integer('batch_size', 256, "The batch size.")
flags.DEFINE_integer('epochs', 50, "The number of epochs.")
flags.DEFINE_string('training_file', '', "Bottleneck features training file (.p)")
flags.DEFINE_string('validation_file', '', "Bottleneck features validation file (.p)")


def load_bottleneck_data(training_file, validation_file):
    """
    Utility function to load bottleneck features.

    Arguments:
        training_file - String
        validation_file - String
    """
    print("Training file", training_file)
    print("Validation file", validation_file)

    with open(training_file, 'rb') as f:
        train_data = pickle.load(f)
    with open(validation_file, 'rb') as f:
        valid_data = pickle.load(f)

    x_train = train_data['features']
    y_train = train_data['labels']
    x_valid = valid_data['features']
    y_valid = valid_data['labels']

    return x_train, y_train, x_valid, y_valid


def main(_):
    # Load bottleneck data
    x_train, y_train, x_valid, y_valid = load_bottleneck_data(FLAGS.training_file, FLAGS.validation_file)

    print(x_train.shape, y_train.shape)
    print(x_valid.shape, y_valid.shape)

    # TODO: Define your model and hyperparams here
    nb_classes = len(np.unique(y_train))
    input_shape = x_train.shape[1:]
    inp = Input(shape=input_shape)
    x = Flatten()(inp)
    x = Dense(nb_classes, activation='softmax')(x)
    model = Model(inp, x)
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    # TODO: Train your model here
    model.fit(x_train, y_train, nb_epoch=FLAGS.epochs, batch_size=FLAGS.batch_size,
              validation_data=(x_valid, y_valid), shuffle=True)


# Parses flags and calls the `main` function above
if __name__ == '__main__':
    tf.app.run()
