import os
import cv2
import numpy as np
import matplotlib.pyplot as plt


def calibrate(image_files: list, grid: tuple) -> tuple:
    """
    Calibrate camera by matching chessboard corners onto their real world 3D coordinates.
    :param image_files: list of chessboard images
    :param grid: inner grid size (width, height)
    :return: (camera matrix, distortion coefficients)
    """
    x, y = np.meshgrid(np.arange(grid[0]), np.arange(grid[1]))
    z = np.zeros(shape=grid[::-1])
    xyz = np.dstack([x, y, z])
    objects = xyz.reshape(-1, 3).astype(np.float32)

    object_pts, image_pts = [], []
    for image_file in image_files:
        image = cv2.imread(image_file)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        retval, corners = cv2.findChessboardCorners(gray, grid)
        if retval:
            image_pts.append(corners)
            object_pts.append(objects)

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(object_pts, image_pts, gray.shape[::-1], None, None)
    return mtx, dist


if __name__ == '__main__':
    pattern_size = (9, 6)
    calibration_dir = 'calibration/'

    # Load list of calibration images
    calibration_image_files = filter(lambda img: img.endswith('.jpg'), os.listdir(calibration_dir))
    calibration_image_files = map(lambda filename: calibration_dir + filename, calibration_image_files)
    calibration_image_files = list(calibration_image_files)

    # Calibrate camera
    mtx, dst = calibrate(calibration_image_files, pattern_size)

    # Plot an undistorted calibration image
    plt.figure()
    subplot = plt.subplot(1, 2, 1)
    subplot.set_title("Original")
    original = cv2.imread(calibration_image_files[0])
    plt.imshow(cv2.cvtColor(original, cv2.COLOR_BGR2RGB))
    subplot.axes.get_xaxis().set_visible(False)
    subplot.axes.get_yaxis().set_visible(False)

    subplot = plt.subplot(1, 2, 2)
    subplot.set_title("Undistorted")
    undistorted = cv2.undistort(original, mtx, dst)
    plt.imshow(cv2.cvtColor(undistorted, cv2.COLOR_BGR2RGB))
    subplot.axes.get_xaxis().set_visible(False)
    subplot.axes.get_yaxis().set_visible(False)
    plt.show(block=False)

    # Plot an undistorted test image
    plt.figure()
    subplot = plt.subplot(1, 2, 1)
    subplot.set_title("Original")
    original = cv2.imread('tests/test1.jpg')
    plt.imshow(cv2.cvtColor(original, cv2.COLOR_BGR2RGB))
    subplot.axes.get_xaxis().set_visible(False)
    subplot.axes.get_yaxis().set_visible(False)

    subplot = plt.subplot(1, 2, 2)
    subplot.set_title("Undistorted")
    undistorted = cv2.undistort(original, mtx, dst)
    plt.imshow(cv2.cvtColor(undistorted, cv2.COLOR_BGR2RGB))
    subplot.axes.get_xaxis().set_visible(False)
    subplot.axes.get_yaxis().set_visible(False)
    plt.show(block=False)

    # Plot the distortion vector field
    plt.figure()
    hticks, wticks = 20, 15
    h, w = original.shape[:2]
    x, y = np.meshgrid(np.linspace(-w/2, +w/2, num=wticks), np.linspace(-h/2, +h/2, num=hticks))
    xy = np.vstack([x.flatten(), y.flatten()]).T.reshape(-1, 1, 2)
    xyu = cv2.undistortPoints(xy, mtx, dst)
    xu, yu = xyu[:, 0, 0].reshape(hticks, wticks), xyu[:, 0, 1].reshape(hticks, wticks)
    subplot = plt.subplot(1, 2, 1)
    subplot.set_title("Distortion Vector Field")
    dx, dy = x - xu, y - yu
    plt.quiver(x, y, dx, dy, pivot='tail', units='xy')
    subplot.axes.get_xaxis().set_visible(False)
    subplot.axes.get_yaxis().set_visible(False)

    subplot = plt.subplot(1, 2, 2)
    subplot.set_title("Distortion Magnitude")
    mag = np.sqrt(dx**2 + dy**2)
    plt.contourf(x, y, mag)
    subplot.axes.get_xaxis().set_visible(False)
    subplot.axes.get_yaxis().set_visible(False)
    plt.show(block=True)

    # Store the camera calibration data
    np.savez('results/calibration.npz', camera_matrix=mtx, distortion_coefficients=dst)
