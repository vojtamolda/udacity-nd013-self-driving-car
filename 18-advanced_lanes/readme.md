
## Lesson 18 - Advanced Lane Finding

<img src="advanced_lanes.png" width="50%" alt="Simulator" />


### Submitted Files & Code

Here's an overview of the content included in the project archive:
 - `readme.md`: Write-up that summarizes the project.
 - `calibration/*`: Images used for camera calibration.
 - `calibration.py`: Code for the camera calibration process.
 - `tests/*`: Input images and videos for the pipeline.
 - `lanes.py`: Lane detection pipeline code
 - `results/*`: Output images and videos as processed by the pipeline.

The `lanes.py` file contains the code for the lane detaction pipeline. `calibration.py` contains the
camera calibration procedure. I did my best to write short, concise and readable functions.
`lanes.py` comes with it's own set of test cases that makes sure everything is working correctly and
also output the images and videos in the `results/` folder.

The `calibration.py` must be run before `lanes.py` since the latter relies on a calibration
coefficients stored by the former.

```sh
python calibration.py
python -m unittest lanes.py
```


### Camera Calibration

The code for this step is contained in the `calibrate(...)` inside of the `calibration.py` file. The
function starts by preparing a regular grid of "object points", which will be the _[x,y,z]_
coordinates of the chessboard corners in the real world coordinate system.

I am assuming the chessboard is fixed on the _[x,y]_ plane at _z=0_, such that the object points
are the same for each calibration image.  Thus, `object_pts` is just a replicated array of
coordinates, and `objects` will be appended with a copy of it every time the
`cv2.findChessboardCorners(...)` successfully detect all chessboard corners in a given image.
`image_pts` are be appended with the _[x,y]_ pixel position of each of the corners in the image
plane after each successful chessboard detection.

I then used the output `object_pts` and `image_pts` to compute the camera calibration and distortion
coefficients using the `cv2.calibrateCamera(...)` function.

I applied this distortion correction to the test image using the `cv2.undistort(...)` function and
obtained this result:

<img src="results/calibration.png" width="80%" alt="Camera Calibration Distorted and Undistorted" />

I was interested in the direction and magnitude of the undistortion function. Displacement of each
pixel can be understood as a vector field where each point has an associated "arrow" that points to
the destination. The following image shows the direction and magnitude of the undistortion vector
field.

<img src="results/calibration-vectors.png" width="80%" alt="Visualization of Camera Calibration" />



### Lane Detection Pipeline

The following schematics display the entire lane detection pipeline. Each of the steps also contains
an example of it's output. The entire pipeline is in the `lanes.py` file. Each of the "boxes" on the
schematisc roughly corresponds to a function.

<img src="results/pipeline.png" width="70%" alt="Schematics of the Entire Lane Detection Pipeline" />

Every step is described in more details in the following sections.

##### Undistortion
To demonstrate this step of the lane detection pipeline, here's an example application to one of the
test images from the `test/` folder:

<img src="results/undistortion.png" width="70%" alt="Example Image Distorted and Undistorted" />


##### Perspective Transform
The code for the perspective transform is in the `perspective_transform(...)` function. The function
takes an image and an output size and outputs a top-down aerial view of the road. There's also an
`inverse` flag that allows it to work in the opposite direction - i.e return a front facing image
from a top-down view of the road.

The source and destination points for the perspective are hardcoded as percentages of the input and
output image sizes. The following table summarizes the mapping:

| Location     | Source [%, %]  | Destination [%, %] |
|:-------------|:--------------:|:------------------:|
| Top Right    | [57, 63]       | [100, 0]           |
| Top Left     | [43, 63]       | [0, 0]             |
| Bottom Right | [-10, 100]     | [100, 100]         |
| Bottom Left  | [110, 100]     | [0, 100]           |

I verified that my perspective transform way displaying the `straight_lines1.jpg` and
`straing_lines2.jpg` images and manually fine tuning the top points source points to achieve
perfectly parallel lines non-diverging left and right lane. My original plan was to get coordinate
of pixels that are on the lane and make them


##### Sobel Filter & Color Masking
The color masking is performed after the top-down view of the road is converted to HSV colorspace.
I use two mcolor masks to detect the pixels that likely belong to a lane marking. One mask is for
whites and the other to detect shades of yellows on the image. The implementation is in the
`color_mask(...)` function. It's a thin wrapper around the `cv2.inRange(...)` function.

The Sobel filter does a gradient calculation in the horizontal direction to detect changes in the
V-channel that are likely to be a color step from the gray asphalt to the lane marking.
The implementation in the `sobel_filter(...)` takes lower and upper gradient threshold that will
be included in the output mask.

At the end all of the the white, yellow and Sobel maks are binary OR-ed together into the final
lane pixel mask.

##### Polynomial Fitting
I split the mask of pixels belonging to lanes to left and right half of the top-down image and then
I fit a 2nd degree polynomial to each respective half. The fitting is done in the
`detect_lanes(...)` function and contains detection for cases where no lane pixels are detected.
The input data to the fit are _[x,y]_ coordinates of the pixels that are masked as belonging
to a lane.

##### Curve Radius and Lane Departure
The following image shows the way I calculate the curvature and also defines points used in the
next discussion paragraph:

<img src="results/curvature.png" width="60%" alt="Calculation of Curve Radius and Lane Departure" />

Generally, each of the polynomial has a different radius of curvature at each point, so one needs
to choose a point where to do the calculation. I decided to use the mid-point in the y direction
(points _R1_ and _L1_), since the shape of the polynomial here is influenced by the points both
above and below and is more robust to noise. However, this technically isn't the radius of the
curvature the vehicle is going through at the moment, but rather a radius it will take in the next
few moments.

Calculation of the curve radius is a little bit tricky since the fitted polynomials operate in
pixel coordinates. If the polynomials were in meters one could simply calculate the first and second
derivative and calculate the curvature according to the formula from
[Wikipedia](https://en.wikipedia.org/wiki/Curvature).

One way to go about the unit conversion problem would be to convert the mask pixel coordinates to
meters and re-fit the polynomials. With a little bit of calculus and algebra one can arrive at a
not exactly intuitive equation that uses the _Kx_ and _Ky_ pixel scaling factors:

<img src="results/curvature-formula.png" width="60%" alt="Formula to Calcualte the Curve Radius" />

The scaling of the curve radius _R_ is non-linear and naturally depends on the ratio of _Kx_ and
_Ky_. The calculation of curvature is implemented as an extension to the `numpy.Polynomial` class.
There's a new function `curvature(...)` that takes an optional tuple of scaling factors and a point.

To get the exact value of _Kx_ pixel scaling factor one can use the dimension of standardized highway
lane marking from [here](http://regulations.delaware.gov/register/july2011/final/mutcd/Part3.html)
and use the corresponding pixel distance to get the conversion ratio. Dashed lane markings are
_10ft_ long and _20ft_ which yields about _Ky=15px/m_.

Similar estimate can be done, for the horizontal direction. According to
[this](https://safety.fhwa.dot.gov/geometric/pubs/mitigationstrategies/chapter3/3_lanewidth.cfm)
highway lanes are typically -12ft_ wide, which gives about _Kx=100px/m_.

Lane departure is calculated as the pixel difference between points _C_ and _I_ multiplied by _Kx_.
_I_ is the horizontal mid-point of the image and _C_ is the center of detected lane. _C_ is
calculated as an between _L2_ and _R2_.


##### Final Assembly
The final assembly performs inverse perspective transform to overlay the detected lanes onto the
original input image. For debugging purposes I also add the top-down view of the road and lane
pixel mask above the output image. The code is done in the `result_compose(...)` function.

The higher level code of the entire pipeline is in the function `pipeline_run(...)` that takes
the RGB input image and spits out the same image with lane markings rendered on top of it. Here's
an example final output image:

<img src="results/test4.jpg" width="50%" alt="Example Final Output" />



### Discussion

The current pipeline doesn't perform too well on the harder video and fails completely on the harder
video.

The problems with the harder video could be mitigated by using a more sophisticated Sobel filtering.
My Sobel gradient detection is not particularly robust. In the harder video the lane marking
colors are not particularly bright and using the color gradient will help quite a lot here. Also
some kind of time averaging over the several frames could provide less jittery output. The current
pipeline doesn't do any averaging since it seems to work well enough without it on the project
video.

In order to make the pipeline work for the challenge video one would probably have to widen the
perspective transformation window (besides other magic tricks :), since the turns there are so sharp
that they frequently go out of the field of view of the current implementation.

As a genral conclusion, it is easy to create a simple algorithm that performs relatively well on a
few videos, but it is very very hard to create a pipeline that will perform well under all
circumstances.