import os
import cv2
import unittest
import numpy as np
import numpy.polynomial


class Polynomial(numpy.polynomial.Polynomial):
    def curvature(self, x, scale=(1, 1)):
        """
        Calculate curvature (i.e. inverse of radius 1/R) of the polynomial at point x.
        :param x: Point where the curvature is calculated
        :return: Curvature of the polynomial at point x
        """
        K = scale[0] / scale[1]
        diff1 = self.deriv()
        diff2 = diff1.deriv()
        return K*diff2(x) / (1 + (K*diff1(x))**2) ** (3/2)


def undistort(image: np.array) -> np.array:
    """
    Apply camera lens correction/undistortion to the image. Previously saved distortion coefficients
    are loaded from 'results/calibration.npz` numpy file.
    :param image: Distorted image
    :return: Undistorted image
    """
    try:
        undistorted = cv2.undistort(image, undistort.mtx, undistort.dst)
    except AttributeError:
        data = np.load('results/calibration.npz')
        undistort.mtx = data['camera_matrix']
        undistort.dst = data['distortion_coefficients']
        undistorted = cv2.undistort(image, cameraMatrix=undistort.mtx, distCoeffs=undistort.dst)
    return undistorted


def perspective_transform(image: np.array, output_shape: tuple, invert: bool=False) -> np.array:
    """
    Perform perspective transformation of a front facing image from the vehicle to an aerial
    top-down helicopter view of the road.
    :param image: Input image
    :param output_shape: Size of the output (height, width)
    :param invert: Perform inverted perspective transform (top down -> front facing)
    :return: Top-down view of the road
    """
    if invert is False:
        trapezoid_shape, rectangle_shape = image.shape[:2], output_shape
    else:
        trapezoid_shape, rectangle_shape = output_shape, image.shape[:2]

    top_right = [0.57 * trapezoid_shape[1], 0.63 * trapezoid_shape[0]]
    top_left = [0.43 * trapezoid_shape[1], 0.63 * trapezoid_shape[0]]
    bottom_right = [1.10 * trapezoid_shape[1], 1.00 * trapezoid_shape[0]]
    bottom_left = [-0.10 * trapezoid_shape[1], 1.00 * trapezoid_shape[0]]
    trapezoid = np.array([top_right, top_left, bottom_left, bottom_right], dtype=np.float32)
    top_right = [rectangle_shape[1], 0]
    top_left = [0, 0]
    bottom_right = [rectangle_shape[1], rectangle_shape[0]]
    bottom_left = [0, rectangle_shape[0]]
    rectangle = np.array([top_right, top_left, bottom_left, bottom_right], dtype=np.float32)

    if invert is False:
        persp_mtx = cv2.getPerspectiveTransform(src=trapezoid, dst=rectangle)
        top_down = cv2.warpPerspective(image, M=persp_mtx, dsize=output_shape[::-1], flags=cv2.INTER_CUBIC)
        return top_down
    else:
        persp_mtx = cv2.getPerspectiveTransform(src=rectangle, dst=trapezoid)
        front_facing = cv2.warpPerspective(image, M=persp_mtx, dsize=output_shape[::-1], flags=cv2.INTER_CUBIC)
        return front_facing


def sobel_filter(hsv: np.array, threshold: tuple) -> np.array:
    """
    Apply Sobel filter threshold to an image. The output mask is non-zero if the normalized Sobel gradient of
    the V channel lies within the lower and upper threshold.
    :param hsv: Input image converted to HSV colorspace
    :param threshold: Tuple of lower and upper bound for the absolute value of Sobel filter output (0-255)
    :return: Binary mask
    """
    sobelx = cv2.Sobel(hsv[:, :, 2], cv2.CV_64F, 1, 0, ksize=5)
    sobelx = np.absolute(sobelx)
    sobelx = np.uint8(255 * sobelx / sobelx.max())
    sobel_indices = (threshold[0] <= sobelx) & (sobelx <= threshold[1])
    mask = np.zeros(shape=hsv.shape[:2], dtype=np.uint8)
    mask[sobel_indices] = 255
    return mask


def color_mask(hsv: np.array, threshold: tuple) -> np.array:
    """
    Apply color selection mask to an image. The output mask is non-zero only for pixels that lie within each
    of the hue, saturation and value ranges.
    :param hsv: Input image converted to HSV colorspace
    :param threshold: Tuple of lower and upper color bound (0-360 // 2, 0-255, 0-255)
    :return: Binary mask
    """
    mask = cv2.inRange(hsv, lowerb=np.array(threshold[0]), upperb=np.array(threshold[1]))
    return mask


def detect_lanes(mask: np.array) -> (Polynomial, Polynomial):
    """
    Fit a 2nd degree polynomial to the top-down view of lane pixels. The left and right lanes are fitted
    to the respective halves of the image.
    :param mask: Mask of pixels belonging to left and right lanes
    :return: Tuple of left and right lane polynomials fitted to the lane pixels
    """
    height, width = mask.shape
    y, x = np.where(mask[:, :])
    left_filter, right_filter = (x < width/2), (width/2 <= x)
    left_poly, right_poly = Polynomial([0, 0, 0]), Polynomial([mask.shape[1], 0, 0])
    if left_filter.sum() > 0:
        left_poly = Polynomial.fit(y[left_filter], x[left_filter], deg=2)
    if right_filter.sum() > 0:
        right_poly = Polynomial.fit(y[right_filter], x[right_filter], deg=2)
    return left_poly, right_poly


def draw_lanes(image: np.array, left_poly: Polynomial, right_poly: Polynomial, center: bool=False) -> np.array:
    """
    Draw left and right lane polynomials into the image top-down road image. If center is True the area in
    between lines is painted too. Left lane is green, right lane is red and the center are is white.
    :param image: Top-down view of the road
    :param left_poly: Left lane polynomial
    :param right_poly: Right lane polynomial
    :param center: Flag to draw the central area in between the lines
    :return: Image with rendered lanes
    """

    height, width = image.shape[:2]
    left_y, right_y = np.linspace(0, height, 20), np.linspace(height, 0, 20)
    left_x, right_x = left_poly(left_y),  right_poly(right_y)
    left, right = np.vstack([left_x, left_y]), np.vstack([right_x, right_y])
    left, right = np.int32(left.T), np.int32(right.T)

    if center is True:
        cv2.fillPoly(image, pts=[np.vstack([left, right])], color=[255, 255, 255])
    cv2.polylines(image, pts=[left], thickness=10, isClosed=False, color=[255, 0, 0])
    cv2.polylines(image, pts=[right], thickness=10, isClosed=False, color=[0, 255, 0])
    return image


def result_compose(undistorted: np.array, top_down: np.array, mask: np.array,
                   left_poly: Polynomial, right_poly: Polynomial) -> np.array:
    """
    Compose the final input image from images of the various stages of the pipeline.
    :param undistorted: Front facing undistorted image
    :param top_down: Top-down aerial view of the road ahead
    :param mask: Mask with lane marking pixels
    :param left_poly: Left lane polynomial
    :param right_poly: Right lane polynomial
    :return: Final output image
    """
    top_down = draw_lanes(top_down, left_poly, right_poly, center=False)
    mask = cv2.cvtColor(mask, code=cv2.COLOR_GRAY2BGR)
    top_row = np.hstack([top_down, mask])

    px2meters = (1/15, 1/100)  # [meter/px]
    height, width = top_down.shape[0], top_down.shape[1]
    radius_l = 1/left_poly.curvature(height/2, scale=px2meters)
    radius_r = 1/right_poly.curvature(height/2, scale=px2meters)
    off_center = (width/2 - (right_poly(height) + left_poly(height))/2) * px2meters[1]
    print("L: {:+.0f} m | R: {:+.0f} m | O: {:+.2f} m".format(radius_l, radius_r, off_center))

    lanes = draw_lanes(np.zeros(shape=top_down.shape, dtype=np.uint8), left_poly, right_poly, center=True)
    front_facing = perspective_transform(lanes, output_shape=undistorted.shape[:2], invert=True)
    bottom_row = cv2.addWeighted(src1=undistorted, alpha=1.0, src2=front_facing, beta=0.4, gamma=0.0)

    composed = np.vstack([top_row, bottom_row])
    return composed


def pipeline_run(image: np.array) -> np.array:
    """
    Detect lanes in image and output detected lanes rendered over the original image. Curve radius
    and lane departure are printed to the console for each processed image.
    :param image: Input image converted to RGB colorspace.
    :return: Input image with detected left and right lanes overlaid on it.
    """
    undistorted = undistort(image)
    top_down = perspective_transform(undistorted, output_shape=(320, 640), invert=False)
    hsv = cv2.cvtColor(top_down, code=cv2.COLOR_RGB2HSV)

    white_mask = color_mask(hsv, threshold=([0, 0, 230], [360 // 2, 30,  255]))
    yellow_mask = color_mask(hsv, threshold=([35 // 2, 60, 165], [65 // 2, 255,  255]))
    sobel_mask = sobel_filter(hsv, threshold=[80, 255])
    mask = (yellow_mask | white_mask | sobel_mask)

    left_poly, right_poly = detect_lanes(mask)

    overall = result_compose(undistorted, top_down, mask, left_poly, right_poly)
    return overall


class AdvancedLaneDetection(unittest.TestCase):
    tests_dir = 'tests/'
    results_dir = 'results/'

    def test_images(self):
        for image_file in filter(lambda img: img.endswith('.jpg'), os.listdir(self.tests_dir)):
            input = cv2.imread(self.tests_dir + image_file)
            input = cv2.cvtColor(input, code=cv2.COLOR_BGR2RGB)
            result = pipeline_run(input)
            result = cv2.cvtColor(result, code=cv2.COLOR_RGB2BGR)
            cv2.imwrite(self.results_dir + image_file, result)

    def test_videos(self):
        import moviepy.editor as mpy
        for video_file in filter(lambda img: img.endswith('.mp4'), os.listdir(self.tests_dir)):
            input = mpy.VideoFileClip(self.tests_dir + video_file)
            result = input.fl_image(pipeline_run)
            result.preview(audio=False)
            result.write_videofile(self.results_dir + video_file, codec='mpeg4', audio=False)
