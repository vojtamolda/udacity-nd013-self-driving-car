import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from tensorflow.contrib.layers import flatten
from tensorflow.examples.tutorials.mnist import input_data


# Load MNIST digits dataset
mnist = input_data.read_data_sets("mnist/", reshape=False)
train_x, train_y = shuffle(mnist.train.images, mnist.train.labels)
valid_x, valid_y = shuffle(mnist.validation.images, mnist.validation.labels)
test_x, test_y = shuffle(mnist.test.images, mnist.test.labels)

# LeNet accepts 32x32 images
train_x = np.pad(train_x, ((0,0), (2,2), (2,2), (0,0)), 'constant')
valid_x = np.pad(valid_x, ((0,0), (2,2), (2,2), (0,0)), 'constant')
test_x = np.pad(test_x, ((0,0), (2,2), (2,2), (0,0)), 'constant')

# Display a sample from the dataset
figure = plt.figure()
for i in range(16):
    subplot = figure.add_subplot(4, 4, i + 1)
    subplot.set_title(str(train_y[i]))
    subplot.axes.get_xaxis().set_visible(False)
    subplot.axes.get_yaxis().set_visible(False)
    plt.imshow(train_x[i].squeeze(), cmap='gray_r')
plt.show()

# Features and labels
x = tf.placeholder(tf.float32, [None, 32, 32, 1])
y = tf.placeholder(tf.int32, [None])
y_hot = tf.one_hot(y, 10)


# LeNet-5 CNN architecture
def lenet(image):
    # Parameters for normally distributed initial weights
    mean, stddev = 0, 0.1

    # Layer 1: Convolutional. Input = 32x32x1. Output = 28x28x6. Filter = 5x5.
    filter1 = tf.Variable(tf.truncated_normal([5, 5, 1, 6], mean=mean, stddev=stddev))
    bias1 =  tf.Variable(tf.zeros([6]))
    layer1 = tf.nn.conv2d(image, filter1, strides=[1, 1, 1, 1], padding='VALID') + bias1
    layer1 = tf.nn.sigmoid(layer1)
    # Pooling. Input = 28x28x6. Output = 14x14x6. Kernel = 2x2.
    pool1 = tf.nn.max_pool(layer1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

    # Layer 2: Convolutional. Input = 14x14x6. Output = 10x10x16. Filter = 5x5.
    filter2 = tf.Variable(tf.truncated_normal([5, 5, 6, 16], mean=mean, stddev=stddev))
    bias2 = tf.Variable(tf.zeros([16]))
    layer2 = tf.nn.conv2d(pool1, filter2, strides=[1, 1, 1, 1], padding='VALID') + bias2
    layer2 = tf.nn.sigmoid(layer2)
    # Pooling. Input = 10x10x16. Output = 5x5x16. Kernel = 2x2.
    layer2 = tf.nn.max_pool(layer2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')
    layer2 = flatten(layer2)

    # Layer 3: Fully Connected. Input = 400. Output = 120.
    weight3 = tf.Variable(tf.truncated_normal([400, 120], mean=mean, stddev=stddev))
    bias3 = tf.Variable(tf.zeros([120]))
    layer3 = tf.matmul(layer2, weight3) + bias3
    layer3 = tf.nn.sigmoid(layer3)

    # Layer 4: Fully Connected. Input = 120. Output = 84.
    weight4 = tf.Variable(tf.truncated_normal([120, 84], mean=mean, stddev=stddev))
    bias4 = tf.Variable(tf.zeros([84]))
    layer4 = tf.matmul(layer3, weight4) + bias4
    layer4 = tf.nn.sigmoid(layer4)

    # Layer 5: Fully Connected. Input = 84. Output = 10.
    weight5 = tf.Variable(tf.truncated_normal([84, 10], mean=mean, stddev=stddev))
    bias5 = tf.Variable(tf.zeros([10]))
    layer5 = tf.matmul(layer4, weight5) + bias5
    logits = layer5

    return logits


# Hyperparameters
epochs = 15
batch_size = 400
learning_rate = 0.001

# Initialization dictionaries
train_dict = {x: train_x, y: train_y}
valid_dict = {x: valid_x, y: valid_y}
test_dict = {x: test_x, y: test_y}

# Loss, accuracy and optimizer
logits = lenet(x)
cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits, y_hot)
loss = tf.reduce_mean(cross_entropy)
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)
correct_prediction = tf.equal(tf.argmax(logits, axis=1), tf.argmax(y_hot, axis=1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
saver = tf.train.Saver()

# Plot storage
plt_epoch, plt_loss, plt_train_accuracy, plt_valid_accuracy = [], [], [], []


# Train the network
with tf.Session() as session:
    session.run(tf.global_variables_initializer())

    # Training cycles
    for epoch in range(epochs):
        for batch, start in enumerate(range(0, train_x.shape[0], batch_size)):
            # Get a batch of training features and labels
            stop = min(start + batch_size, train_x.shape[0])
            batch_dict = {x: train_x[start:stop], y: train_y[start:stop]}
            # Run optimizer and get loss
            _, current_loss = session.run([optimizer, loss], feed_dict=batch_dict)

        # Calculate training and validation accuracy for plotting
        train_accuracy = session.run(accuracy, feed_dict=batch_dict)
        valid_accuracy = session.run(accuracy, feed_dict=valid_dict)
        plt_epoch.append(epoch)
        plt_loss.append(current_loss)
        plt_train_accuracy.append(train_accuracy)
        plt_valid_accuracy.append(valid_accuracy)
        print("Epoch {}/{}".format(epoch, epochs))

    # Save the trained model
    saver.save(session, 'mnist/lenet.ckpt')

    # Final validation accuracy
    test_accuracy = session.run(accuracy, feed_dict=test_dict)
    print("Final test accuracy {:.2%}.".format(test_accuracy))


# Loss and accuracy plots
loss_plot = plt.subplot(2, 1, 1)
loss_plot.set_title('Loss')
loss_plot.plot(plt_epoch, plt_loss, 'g')
loss_plot.set_xlim([plt_epoch[0], plt_epoch[-1]])
acc_plot = plt.subplot(2, 1, 2)
acc_plot.set_title('Accuracy')
acc_plot.plot(plt_epoch, plt_train_accuracy, 'r', label='Training Accuracy')
acc_plot.plot(plt_epoch, plt_valid_accuracy, 'x', label='Validation Accuracy')
acc_plot.set_ylim([0, 1.0])
acc_plot.set_xlim([plt_epoch[0], plt_epoch[-1]])
acc_plot.legend(loc=4)
plt.tight_layout()
plt.show()
