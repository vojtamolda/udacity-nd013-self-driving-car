import numpy as np
from signs import load_dataset, load_names, preprocess_dataset


# Load pickled German traffic signs dataset and label names
train_x, train_y = load_dataset('data/train.p')
test_x, test_y = load_dataset('data/test.p')
names_y = load_names('data/names.csv')


# Preprocess the dataset
from keras.models import Sequential
from keras.layers.convolutional import Convolution2D
from keras.layers.pooling import MaxPooling2D
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.utils import np_utils

train_x, train_y = preprocess_dataset(train_x, train_y)
test_x, test_y = preprocess_dataset(test_x, test_y)
train_y = np_utils.to_categorical(train_y, len(names_y))
test_y = np_utils.to_categorical(test_y, len(names_y))


# Build a neural network classifier layer
def perceptron_layer(model, **kwargs):
    model.add(Flatten(**kwargs))
    model.add(Dense(128))
    model.add(Activation('relu'))
    model.add(Dense(43))
    model.add(Activation('softmax'))
    return model

# Train the neural network classifier
model = Sequential()
model = perceptron_layer(model, input_shape=(32, 32, 1))
model.compile('adam', 'categorical_crossentropy', ['accuracy'])
history = model.fit(train_x, train_y, batch_size=1048, nb_epoch=10, validation_split=0.2)
test_score = model.evaluate(test_x, test_y)
print("Perceptron classifier score : {}".format(test_score))


# Build a convolutional layer with max-pooling and dropout
def convolutional_layer(model, **kwargs):
    model.add(Convolution2D(32, 3, 3, border_mode='valid', **kwargs))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    return model

# Train the convolutional classifier
model = Sequential()
model = convolutional_layer(model, input_shape=(32, 32, 1))
model = perceptron_layer(model)
model.compile('adam', 'categorical_crossentropy', ['accuracy'])
history = model.fit(train_x, train_y, batch_size=1048, nb_epoch=10, validation_split=0.2)
test_score = model.evaluate(test_x, test_y)
print("Convolutional classifier score : {}".format(test_score))
