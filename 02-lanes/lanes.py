import os
import cv2
import math
import unittest
import numpy as np
import scipy.optimize
import moviepy.editor as mpy


def gaussian_blur(img, kernel_size):
    return cv2.GaussianBlur(img, (kernel_size, kernel_size), 0)


def canny(img, low_threshold, high_threshold):
    return cv2.Canny(img, low_threshold, high_threshold)


def region_of_interest(img, vertices):
    mask = np.zeros_like(img)
    if len(img.shape) > 2:
        channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255

    cv2.fillPoly(mask, vertices, ignore_mask_color)
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image


def hough_lines(img, rho, theta, threshold, min_line_len, max_line_gap, min_line_slope):
    return cv2.HoughLinesP(img, rho, theta, threshold, np.array([]),
                           minLineLength=min_line_len, maxLineGap=max_line_gap)


def filter_lines(lines, min_angle, max_angle, absolute=False):
    def slope(line):
        x1, y1, x2, y2 = line[0]
        slope = math.atan2(-(y2 - y1), x2 - x1)
        return abs(slope) if absolute else slope

    if lines is None:
        return None
    filter_iterator = filter(lambda line: min_angle < slope(line) < max_angle, lines)
    return np.array(list(filter_iterator))


def fit_lane(lines):
    def length(line):
        x1, y1, x2, y2 = line[0]
        return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

    def linear(x, slope, offset):
        return x * slope + offset

    if lines is None or len(lines) == 0:
        return None
    weights = np.repeat(list(map(length, lines)), 2)
    points = lines.reshape((2 * lines.shape[0], 2))
    coeffs, covariance = scipy.optimize.curve_fit(linear, xdata=points[:, 0], ydata=points[:, 1],
                                                  sigma=weights)
    return coeffs


class MovingAverage:
    def __init__(self, pseudo_window=20):
        self._window = pseudo_window
        self._value = None

    def reset(self):
        self._value = None

    def average(self):
        return self._value

    def append(self, new):
        if self._value is None:
            self._value = new
        elif new is not None:
            self._value += (new - self._value) / self._window


def draw_lane(img, coeffs, y_lower, y_upper, color=(255, 0, 0), thickness=2):
    def linear_x(y):
        return (y - coeffs[1]) / coeffs[0]

    if coeffs is None:
        return
    x_lower, x_upper = int(linear_x(y_lower)), int(linear_x(y_upper))
    y_lower, y_upper = int(y_lower), int(y_upper)
    cv2.line(img, (x_lower, y_lower), (x_upper, y_upper), color, thickness)


def draw_lines(img, lines, color=(255, 0, 0), thickness=2):
    if lines is None:
        return
    for line in lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(img, (x1, y1), (x2, y2), color, thickness)


def draw_rectangle(img, vertices, color=(255, 0, 0), thickness=2):
    lines = [[[*vertices[0], *vertices[1]]],
             [[*vertices[1], *vertices[2]]],
             [[*vertices[2], *vertices[3]]],
             [[*vertices[3], *vertices[0]]]]
    draw_lines(img, lines, color, thickness)


# -- Pipeline --


left_lane = MovingAverage()
right_lane = MovingAverage()


def pipeline_reset():
    left_lane.reset()
    right_lane.reset()


def pipeline_run(color_image):
    # Apply Gaussian blur
    blurred_image = gaussian_blur(color_image, 7)

    # Perform Canny edge detection algorithm
    canny_image = canny(blurred_image, 50, 150)

    # Crop area of interest
    top_right = [0.60 * color_image.shape[1], 0.60 * color_image.shape[0]]
    top_left = [0.35 * color_image.shape[1], 0.60 * color_image.shape[0]]
    bottom_right = [color_image.shape[1] - 1, 0.95 * color_image.shape[0]]
    bottom_left = [0, 0.95 * color_image.shape[0]]
    vertices = np.array([top_right, top_left, bottom_left, bottom_right], dtype=np.int64)
    cropped_image = region_of_interest(canny_image, [vertices])

    # Detect lines via Hough transform and filter out weird slopes
    lines = hough_lines(cropped_image, 2, math.pi / 180, 15, 40, 20, math.pi / 2)
    filtered_lines = filter_lines(lines, math.radians(20), math.radians(40), absolute=True)

    # Fit left and right lane weighted by length of respective detected lines
    left_lines = filter_lines(filtered_lines, math.radians(20), math.radians(40))
    left_fit = fit_lane(left_lines)
    left_lane.append(left_fit)
    draw_lane(color_image, left_lane.average(), top_left[1], bottom_left[1],
              color=(255, 0, 0), thickness=3)
    right_lines = filter_lines(filtered_lines, math.radians(-40), math.radians(-20))
    right_fit = fit_lane(right_lines)
    right_lane.append(right_fit)
    draw_lane(color_image, right_lane.average(), top_right[1], bottom_right[1],
              color=(255, 0, 0), thickness=3)

    # Draw debugging image
    canny_image = cv2.cvtColor(canny_image, cv2.COLOR_GRAY2RGB)
    draw_rectangle(canny_image, vertices, color=(0, 255, 255), thickness=1)
    draw_lines(canny_image, lines, color=(0, 255, 0), thickness=1)
    draw_lines(canny_image, filtered_lines, color=(255, 0, 0), thickness=2)
    draw_lane(canny_image, left_fit, top_left[1], bottom_left[1],
              color=(255, 0, 255), thickness=1)
    draw_lane(canny_image, right_fit, top_right[1], bottom_right[1],
              color=(255, 0, 255), thickness=1)

    # Merge debugging and output image together
    result = np.vstack((canny_image, color_image))
    return result


# -- Tests --
# Run as python -m unittest lane_detection.py


class LaneDetection(unittest.TestCase):
    tests_dir = 'tests/'
    results_dir = 'results/'

    def test_images(self):
        for image_file in filter(lambda img: img.endswith('.jpg'), os.listdir(self.tests_dir)):
            input = cv2.imread(self.tests_dir + image_file)
            pipeline_reset()
            result = pipeline_run(input)
            cv2.imshow("image", result)
            cv2.imwrite(self.results_dir + image_file, result)

    def test_videos(self):
        for video_file in filter(lambda img: img.endswith('.mp4'), os.listdir(self.tests_dir)):
            input = mpy.VideoFileClip(self.tests_dir + video_file)
            pipeline_reset()
            result = input.fl_image(pipeline_run)
            result.preview(audio=False)
            result.write_videofile(self.results_dir + video_file, codec="mpeg4", audio=False)
