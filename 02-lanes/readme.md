
## Lesson 2 - Finding Lane Lines on the Road

<img src="results/challenge2.jpg" width="50%" alt="Challenge Output" />

My pipeline is a the standard Canny -> Crop -> Hough -> Lanes process presented in the videos with
a couple of tweaks to make it work on the [`challenge.mp4`](tests/challenge.mp4) as well as my own
[`i290Chicago.mp4`](tests/i290Chicago.mp4) video. The schematics displays the entire processing
pipeline:

  <img src="results/pipeline.png" width="70%" alt="Pipeline" />

- I don't do conversion to grayscale. For me, this caused problems with the
  [`challenge.mp4`](tests/challenge.mp4) video, where the lanes had more or less the same shade of
  grey as the surrounding concrete on the road. Canny edge detection works on color images too.

- All the detected Hough lines are filtered based on their slope to remove outliers. Only lines with
  slope between 20 and 40 degrees are kept. Once the outliers are filtered out, two linear functions
  are fitted to the data to get the final lane positions. The fit is weighted by the length of the
  detected Hough line, so longer lines have. Finally, the left and right lanes are averaged over a
  20 frame long pseudo-moving window.

- For debugging and demonstration purposes the pipeline outputs additional image that shows the
  process - detected Canny edges, crop area, filtered and unfiltered Hough lines.

I was curious how the algorithm performs in winter. I live in Chicago and it's December, so I did a
small test run and recorded myself driving on the I-290 W with a dashboard mounted iPhone. There's
a couple of noteworthy things:

- It was -20 C when I drove and the highway developed a sharp change of the tarmac color from light
  to dark gray in areas where cars usually drive. The Canny edge detection algorithm is picking
  these edges and since they are parallel to the lines, the result is offset towards this 'artifact'.

  <img src="results/i290Chicago1.jpg" width="50%" alt="Sharp changes of asphalt color in winter" />

  One way to mitigate this would be to convert the image to grayscale, reduce the overall brightness
  and then do some form of highlighting of white and yellow patches in the image. Since the asphalt
  transition is in shades of gray, this should highlight the edges of lane markings, so the Canny
  edge detection algorithm is more likely to find them.

- The algorithm doesn't detect any lane when I'm driving under the bridge. This happens simply
  because the lanes are not visible. It doesn't seem to be an issue with the algorithm, but with the
  camera itself. iPhone tends to set the exposure to capture the sky color. In the case of a
  self-driving car, the exposure adjusted by the brightness of the lower half of the image makes
  more sense. Over-exposure of the sky hardly matters here compared to a family album for instance.

  <img src="results/i290Chicago2.jpg" width="50%" alt="Failing to detect lines under bridge" />

  I think adding additional step that would normalize the image color range within the cropped are
  is also likely to help with lane detection under bridges.

- The algorithm doesn't work too well in other scenarios than 'normal' highway driving. So I
  restricted the test cases only to this case. For example during exit from the highway completely
  the algorithm completely misses the boundary of the merging left lane since it's outside of the
  cropped area.

  <img src="results/i290Chicago2.jpg" width="50%" alt="Failing to detect when exiting highway" />
