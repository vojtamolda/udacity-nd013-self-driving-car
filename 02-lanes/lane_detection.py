import os
import cv2
import math
import unittest
import numpy as np
import matplotlib as mpl
import moviepy.editor as mpy


def grayscale(img):
    """Applies the Grayscale transform
    This will return an image with only one color channel
    but NOTE: to see the returned image as grayscale
    you should call plt.imshow(gray, cmap='gray')"""
    return cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)


def canny(img, low_threshold, high_threshold):
    """Applies the Canny transform"""
    return cv2.Canny(img, low_threshold, high_threshold)


def gaussian_blur(img, kernel_size):
    """Applies a Gaussian Noise kernel"""
    return cv2.GaussianBlur(img, (kernel_size, kernel_size), 0)


def region_of_interest(img, vertices):
    """
    Applies an image mask.

    Only keeps the region of the image defined by the polygon
    formed from `vertices`. The rest of the image is set to black.
    """
    # defining a blank mask to start with
    mask = np.zeros_like(img)

    # defining a 3 channel or 1 channel color to fill the mask with depending on the input image
    if len(img.shape) > 2:
        channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
        ignore_mask_color = (255,) * channel_count
    else:
        ignore_mask_color = 255

    # filling pixels inside the polygon defined by "vertices" with the fill color
    cv2.fillPoly(mask, vertices, ignore_mask_color)

    # returning the image only where mask pixels are nonzero
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image


def draw_lines(img, lines, color=(255, 0, 0), thickness=2):
    """
    NOTE: this is the function you might want to use as a starting point once you want to
    average/extrapolate the line segments you detect to map out the full
    extent of the lane (going from the result shown in raw-lines-example.mp4
    to that shown in P1_example.mp4).

    Think about things like separating line segments by their
    slope ((y2-y1)/(x2-x1)) to decide which segments are part of the left
    line vs. the right line.  Then, you can average the position of each of
    the lines and extrapolate to the top and bottom of the lane.

    This function draws `lines` with `color` and `thickness`.
    Lines are drawn on the image inplace (mutates the image).
    If you want to make the lines semi-transparent, think about combining
    this function with the weighted_img() function below
    """
    for line in lines:
        for x1, y1, x2, y2 in line:
            cv2.line(img, (x1, y1), (x2, y2), color, thickness)


def hough_lines(img, rho, theta, threshold, min_line_len, max_line_gap, min_line_slope):
    """
    `img` should be the output of a Canny transform.

    Returns an image with hough lines drawn.
    """
    def abs_slope(line):
        slope = 0.0
        for x1, y1, x2, y2 in line:
          slope = (slope + math.fabs(math.atan2(y2 - y1, x2 - x1))) / 2
        return slope

    lines = cv2.HoughLinesP(img, rho, theta, threshold, np.array([]),
                            minLineLength=min_line_len, maxLineGap=max_line_gap)
    line_img = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.uint8)
    draw_lines(line_img, lines)
    return line_img, lines


def weighted_img(img, initial_img, α=0.8, β=1., λ=0.):
    """
    `img` is the output of the hough_lines(), An image with lines drawn on it.
    Should be a blank image (all black) with lines drawn on it.

    `initial_img` should be the image before any processing.

    The result image is computed as follows:

    initial_img * α + img * β + λ
    NOTE: initial_img and img must be the  same shape!
    """
    return cv2.addWeighted(initial_img, α, img, β, λ)


def pipeline(color_image):
    gray_image = grayscale(color_image)

    blurred_image = gaussian_blur(gray_image, 7)

    canny_image = canny(blurred_image, 50, 150)  # 20, 40

    top_right = [0.55 * gray_image.shape[1], 0.60 * gray_image.shape[0]]
    top_left = [0.45 * gray_image.shape[1], 0.60 * gray_image.shape[0]]
    bottom_right = [gray_image.shape[1] - 1, gray_image.shape[0] - 1]
    bottom_left = [0, gray_image.shape[0] - 1]
    vertices = np.array([top_right, top_left, bottom_left, bottom_right], dtype=np.int64)
    cropped_image = region_of_interest(canny_image, [vertices])

    hough_image, lanes = hough_lines(cropped_image, 2, math.pi/180, 15, 40, 20, math.pi/2)

    weighted_image = weighted_img(color_image, hough_image, 1.0, 1.0)
    result = weighted_image

    result = np.vstack((cv2.cvtColor(canny_image, cv2.COLOR_GRAY2RGB), weighted_image))
    return result


class TestLaneDetection(unittest.TestCase):

    images_dir = 'test_images/'
    videos_dir = 'test_videos/'

    def test_images(self):
        for image_file in filter(lambda img: img.endswith('.jpg'), os.listdir(self.images_dir)):
            image = cv2.imread(self.images_dir + image_file)
            result = pipeline(image)
            #both = np.vstack((image, result))
            cv2.imshow("image", result)
            cv2.waitKey(0)

    def test_videos(self):
        for video_file in os.listdir(self.videos_dir):
            input = mpy.VideoFileClip(self.videos_dir + video_file)
            #result = input.fl_image(pipeline)
            #result.preview(fps=15, audio=False)
            #result.write_videofile(video_filename, bitrate="1500k", codec="mpeg4",
            # audio=False)

        pass
