Dear reviewer,

I just noticed that I was supposed to submit only the iPython notebook. I've already done the
project in pure python and I'll copy and paste everything into the notebook if it's necessary.
I'm sorry for the complications and I certainly don't want to waste your time, so here's an
overview of the content:

- `examples/`: Folder with Udacity example results
- `results/`: Images and videos from `tests/` as processed by my pipeline
- `tests/`: Testing images and videos that serve as an input to the pipeline
- `lane_detection.py`: The solution, that should have been a notebook :-)
- `lane_detection.ipynb`: The original Udacity notebook
- `readme.md`: A small essay about the project

To run the pipeline, just execute the test like this: `python -m unittest lane_detection.py`

Thanks and have a good one!

  Vojta
