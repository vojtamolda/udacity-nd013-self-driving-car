import tensorflow as tf


# Create TensorFlow constant tensor
hello_constant = tf.constant('Hello World!')

# Run the session with the hello_constant operation
with tf.Session() as session:
    output = session.run(hello_constant)
    print(output)
