import os
import cv2
import unittest
import numpy as np
import matplotlib.pyplot as plt

# Prevent crashes of GridSearchCV with n_jobs != 1
os.environ['JOBLIB_START_METHOD'] = 'spawn'
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.base import BaseEstimator
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, precision_recall_curve, make_scorer, f1_score

from detect import VehicleDetect, ColorChannelExtractor, SpatialFeatureExtractor, \
                   HistogramFeatureExtractor, HogFeatureExtractor


class VehicleDetectTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.features, cls.labels = VehicleDetect.load_dataset()
        cls.f1scorer = make_scorer(f1_score)

    def test_visualization(self):
        rand_image = self.features[np.random.randint(0, self.features.shape[0])]
        color_extractor = ColorChannelExtractor()
        spatial_extractor = SpatialFeatureExtractor()
        histogram_extractor = HistogramFeatureExtractor()
        hog_extractor = HogFeatureExtractor()

        channel = color_extractor.visualize(rand_image)
        spatial = spatial_extractor.visualize(channel)
        histogram = histogram_extractor.visualize(channel)
        hog = hog_extractor.visualize(channel)

        plt.figure()
        plt.subplot(1, 5, 1)
        plt.title("Original Image")
        plt.imshow(cv2.cvtColor(rand_image, code=cv2.COLOR_BGR2RGB))
        plt.subplot(1, 5, 2)
        plt.title("Single Channel")
        plt.imshow(channel.squeeze(), cmap='gray')
        plt.subplot(1, 5, 3)
        plt.title("Spatial Binning")
        plt.imshow(spatial, cmap='gray', interpolation='nearest')
        plt.subplot(1, 5, 4, aspect=histogram_extractor.bins/max(histogram))
        plt.title("Histogram")
        plt.bar(range(histogram_extractor.bins), histogram)
        plt.subplot(1, 5, 5)
        plt.title("HOG")
        plt.imshow(hog, cmap='gray', interpolation='nearest')
        plt.show(block=True)

    def test_spatial_optimize(self):
        spatial_pipeline = self.optimal_spatial()
        x_train, x_test, y_train, y_test = train_test_split(self.features, self.labels, random_state=0)
        param_grid = {'color__space': ['RGB', 'HSV', 'YUV'],
                      'color__dimensions': [0, 1, 2],
                      'spatial__dsize': [(32, 32), (16, 16), (8, 8), (4, 4)],
                      'svm__C': [1e-4, 1e-3, 1e-2, 1e-1]}
        grid_search = GridSearchCV(spatial_pipeline, param_grid=param_grid, scoring=self.f1scorer,
                                   n_jobs=-1, cv=3, verbose=10, refit=True)
        search_results = grid_search.fit(x_train[:5000], y_train[:5000])
        best_spatial_pipeline = search_results.best_estimator_
        self.stats(best_spatial_pipeline, x_test, y_test)

    def optimal_spatial(self):
        steps = [('color', ColorChannelExtractor(space='HSV', channel=2)),
                 ('spatial', SpatialFeatureExtractor(dsize=(20, 20))),
                 ('scale', StandardScaler(copy=False)),
                 ('svm', LinearSVC(C=0.001))]
        return Pipeline(steps=steps)

    def test_spatial_tradeoff(self):
        from collections import OrderedDict
        spatial_pipeline = self.optimal_spatial()
        x_train, x_test, y_train, y_test = train_test_split(self.features, self.labels, random_state=0)
        f1score_tradeoff = OrderedDict()
        for size in [2, 4, 8, 10, 12, 16, 24, 32, 48, 64]:
            spatial_pipeline.named_steps['spatial'].dsize = (size, size)
            spatial_pipeline.fit(x_train[:5000], y_train[:5000])
            y_pred = spatial_pipeline.predict(x_test)
            f1score_tradeoff[size**2] = f1_score(y_pred, y_test)

        plt.figure()
        plt.title("Spatial Binning F1-Score vs Number of Features Curve")
        plt.xlabel("Number of Spatially Binned Features")
        plt.ylabel("F1-Score")
        plt.plot(list(f1score_tradeoff.keys()), list(f1score_tradeoff.values()), '-o')
        plt.show(block=True)

    def test_histogram_optimize(self):
        histogram_pipeline = self.optimal_histogram()
        x_train, x_test, y_train, y_test = train_test_split(self.features, self.labels, random_state=0)
        param_grid = {'color__space': ['RGB', 'HSV', 'YUV'],
                      'color__dimensions': [0, 1, 2],
                      'histogram__bins': [64, 32, 16, 8],
                      'svm__C': [1e-1, 1e0, 1e+1, 1e+2]}
        grid_search = GridSearchCV(histogram_pipeline, param_grid=param_grid, scoring=self.f1scorer,
                                   n_jobs=-1, cv=3, verbose=10, refit=True)
        search_results = grid_search.fit(x_train[:5000], y_train[:5000])
        best_spatial_pipeline = search_results.best_estimator_
        self.stats(best_spatial_pipeline, x_test, y_test)

    def optimal_histogram(self):
        steps = [('color', ColorChannelExtractor(space='RGB', channel=2)),
                 ('histogram', HistogramFeatureExtractor(bins=64)),
                 ('scale', StandardScaler(copy=False)),
                 ('svm', LinearSVC(C=10))]
        return Pipeline(steps=steps)

    def test_histogram_tradeoff(self):
        from collections import OrderedDict
        histogram_pipeline = self.optimal_histogram()
        x_train, x_test, y_train, y_test = train_test_split(self.features, self.labels, random_state=0)
        f1score_tradeoff = OrderedDict()
        for size in [4, 8, 12, 16, 24, 32, 48, 64, 128]:
            histogram_pipeline.named_steps['histogram'].bins = size
            histogram_pipeline.fit(x_train[:5000], y_train[:5000])
            y_pred = histogram_pipeline.predict(x_test)
            f1score_tradeoff[size] = f1_score(y_pred, y_test)

        plt.figure()
        plt.title("Histogram F1-Score vs Number of Features Curve")
        plt.xlabel("Number of Histogram Bins")
        plt.ylabel("F1-Score")
        plt.plot(list(f1score_tradeoff.keys()), list(f1score_tradeoff.values()), '-o')
        plt.show(block=True)

    def test_hog_optimize(self):
        hog_pipeline = self.optimal_hog()
        x_train, x_test, y_train, y_test = train_test_split(self.features, self.labels, random_state=0)
        param_grid = {'color__space': ['RGB', 'HSV', 'YUV'],
                      'color__dimensions': [0, 1, 2],
                      'hog__orientations': [10, 12, 14],
                      'hog__cells_per_block': [(4, 4), (3, 3), (2, 2)],
                      'hog__pixels_per_cell': [(8, 8), (6, 6), (4, 4)],
                      'svm__C': [1e-4, 1e-3, 1e-2, 1e-1]}
        grid_search = GridSearchCV(hog_pipeline, param_grid=param_grid, scoring=self.f1scorer,
                                   n_jobs=-1, cv=2, verbose=10, refit=True)
        search_results = grid_search.fit(x_train[:2000], y_train[:2000])
        best_hog_pipeline = search_results.best_estimator_
        self.stats(best_hog_pipeline, x_test, y_test)

    def test_hog_tradeoff(self):
        from itertools import product
        from collections import OrderedDict
        hog_pipeline = self.optimal_hog()
        x_train, x_test, y_train, y_test = train_test_split(self.features, self.labels, random_state=0)
        x_train, x_test, y_train, y_test = x_train[:5000], x_test[:1000], y_train[:5000], y_test[:1000]
        f1score_tradeoff = OrderedDict()

        orientations = [8, 10, 12, 14]
        cells_per_block = [(4, 4), (3, 3), (2, 2)]
        pixels_per_cell = [(8, 8), (6, 6), (4, 4)]
        for ori, cpb, ppc in product(orientations, cells_per_block, pixels_per_cell):
            hog_pipeline.named_steps['hog'].orientations = ori
            hog_pipeline.named_steps['hog'].cells_per_block = cpb
            hog_pipeline.named_steps['hog'].pixels_per_cell = ppc
            hog_pipeline.fit(x_train, y_train)
            y_pred = hog_pipeline.predict(x_test)
            num_features = hog_pipeline.named_steps['hog'].transform([x_train[0, :, :, 0]]).size
            f1score_tradeoff[num_features] = f1_score(y_pred, y_test), (ori, cpb, ppc)

        plt.figure()
        plt.title("HOG F1-Score vs Number of Features Curve")
        plt.xlabel("Number of HOG Features [Orientations, Cells/Block, Pixels/Cell]")
        plt.ylabel("F1-Score")
        x = [num_features for num_features, (f1score, params) in f1score_tradeoff.items()]
        y = [f1score for num_features, (f1score, params) in f1score_tradeoff.items()]
        c = [params[0] for num_features, (f1score, params) in f1score_tradeoff.items()]
        plt.scatter(x, y, c=c, s=50)
        for num_features, (f1score, params) in f1score_tradeoff.items():
            plt.annotate(str([params[0], params[1][0], params[2][0]]), xy=(num_features, f1score),
                         xytext=(20, -20), textcoords='offset points', ha='left', va='bottom',
                         bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
                         arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'))
        plt.show(block=True)

    def optimal_hog(self):
        steps = [('color', ColorChannelExtractor(space='HSV', channel=2)),
                 ('hog', HogFeatureExtractor(orientations=12, cells_per_block=(4, 4),
                                             pixels_per_cell=(8, 8))),
                 ('scale', StandardScaler(copy=False)),
                 ('svm', LinearSVC(C=0.001))]
        return Pipeline(steps=steps)

    def test_detector_optimize(self):
        x_train, x_test, y_train, y_test = train_test_split(self.features, self.labels, random_state=0)
        detector = VehicleDetect()
        param_grid = {'svm__C': [1e-1, 1e0, 1e+1, 1e+2],
                      'svm__gamma': [1e-2, 1e-3, 1e-4]}
        grid_search = GridSearchCV(detector.pipeline, param_grid=param_grid, scoring=self.f1scorer,
                                   n_jobs=-1, cv=2, verbose=10, refit=True)
        search_results = grid_search.fit(x_train, y_train)
        best_detector_pipeline = search_results.best_estimator_
        self.stats(best_detector_pipeline, x_test, y_test)

    def test_precision_recall(self):
        x_train, x_test, y_train, y_test = train_test_split(self.features, self.labels, random_state=0)

        spatial_pipeline = self.optimal_spatial()
        spatial_pipeline.fit(x_train, y_train)
        spatial_stats = self.stats(spatial_pipeline, x_test, y_test)

        histogram_pipeline = self.optimal_histogram()
        histogram_pipeline.fit(x_train, y_train)
        histogram_stats = self.stats(histogram_pipeline, x_test, y_test)

        hog_pipeline = self.optimal_hog()
        hog_pipeline.fit(x_train, y_train)
        hog_stats = self.stats(hog_pipeline, x_test, y_test)

        detector = VehicleDetect.load()
        detector_stats = self.stats(detector.pipeline, x_test, y_test)

        plt.figure()
        plt.title("Precision vs Recall Curves of Features")
        plt.ylabel("1 - Precision")
        plt.xlabel("1 - Recall")
        plt.hold(True)
        plt.loglog(1 - spatial_stats[0], 1 - spatial_stats[1], label="Spatial")
        plt.loglog(1 - histogram_stats[0], 1 - histogram_stats[1], label="Histogram")
        plt.loglog(1 - hog_stats[0], 1 - hog_stats[1], label="HOG")
        plt.loglog(1 - detector_stats[0], 1 - detector_stats[1], label="Final Detector")
        plt.hold(False)
        plt.legend(loc="lower left")
        plt.show(block=True)

    def stats(self, classifier: BaseEstimator, x_test: np.array, y_test: np.array):
        y_pred = classifier.predict(x_test)
        y_score = classifier.decision_function(x_test)
        precision, recall, thresholds = precision_recall_curve(y_test, y_score)

        print(classification_report(y_test, y_pred))
        print(classifier.get_params())
        return precision, recall
