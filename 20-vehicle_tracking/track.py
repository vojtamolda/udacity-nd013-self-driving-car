import cv2
import scipy
import numpy as np
import matplotlib.pyplot as plt

from search import VehicleSearch


class VehicleTrack:
    def __init__(self, image_shape: tuple=(720, 1280)):
        self.heatmap = np.zeros(shape=image_shape, dtype=np.float)
        self.search = VehicleSearch()

    def threshold(self):
        abs_threshold, rel_threshold = 15, 0.40 * self.heatmap.max()
        self.heatmap[(self.heatmap < rel_threshold) & (self.heatmap < abs_threshold)] = 0

    def reset(self):
        self.heatmap[:] = 0

    def identify(self):
        self.threshold()
        labels, num_labels = scipy.ndimage.measurements.label(self.heatmap)
        objects = scipy.ndimage.measurements.find_objects(labels, num_labels)

        indentified_cars = []
        for object in objects:
            xtopleft, ytopleft = object[1].start, object[0].start
            xbottomright, ybottomright = object[1].stop, object[0].stop
            indentified_cars.append(((xtopleft, ytopleft), (xbottomright, ybottomright)))
        return indentified_cars

    def track(self, road_image: np.array) -> np.array:
        self.heatmap *= 0.5
        car_windows = self.search.search(road_image)
        for window in car_windows:
            self.heatmap[window[0][1]:window[1][1], window[0][0]:window[1][0]] += 1

        hmap = (self.heatmap/self.heatmap.max()*255).astype(np.uint8)
        hmap = cv2.cvtColor(hmap, code=cv2.COLOR_GRAY2RGB)
        result = cv2.addWeighted(src1=road_image, alpha=1.0, src2=hmap, beta=1.0, gamma=0.0)

        indentified_cars = self.identify()
        result = self.search.visualize(result, indentified_cars)
        return result

