import cv2
import itertools
import numpy as np

from detect import VehicleDetect


class VehicleSearch:
    subscales = [
        {'offset': (5, 400), 'repeat': (131, 15), 'size': (100, 100), 'overlap': (0.9, 0.9)}
    ]

    def __init__(self):
        self.detector = VehicleDetect.load()

    def windows(self, offset: tuple=(0, 0), repeat=(5, 5), size: tuple=(64, 64), overlap: tuple=(0.5, 0.5)):
        xstep, ystep = self.windows_step(size, overlap)
        retval = []
        for ix in range(repeat[0]):
            for iy in range(repeat[1]):
                xtopleft = offset[0] + ix * xstep
                ytopleft = offset[1] + iy * ystep
                xbottomright = xtopleft + size[0]
                ybottomright = ytopleft + size[1]
                retval.append(((xtopleft, ytopleft), (xbottomright, ybottomright)))
        return retval

    def windows_step(self, size: tuple, overlap: tuple) -> tuple:
        xstep = np.int(size[0] * (1 - overlap[0]))
        ystep = np.int(size[1] * (1 - overlap[1]))
        return xstep, ystep

    def windows_size(self, repeat: tuple, size: tuple, overlap: tuple) -> tuple:
        xstep, ystep = self.windows_step(size, overlap)
        xsize = size[0] + (repeat[0] - 1) * xstep
        ysize = size[1] + (repeat[1] - 1) * ystep
        return xsize, ysize

    def search(self, road_image):
        car_windows = []
        for subscale in self.subscales:
            frame_offset = subscale['offset']
            frame_size = self.windows_size(subscale['repeat'], subscale['size'], subscale['overlap'])
            frame_image = road_image[frame_offset[1]:frame_offset[1] + frame_size[1],
                                     frame_offset[0]:frame_offset[0] + frame_size[0]]
            frame_windows =  self.windows(**subscale)
            target_size = self.windows_size(subscale['repeat'], self.detector.input_shape, subscale['overlap'])
            target_image = cv2.resize(frame_image, dsize=target_size, interpolation=cv2.INTER_LINEAR)
            target_windows = self.windows(offset=(0, 0), repeat=subscale['repeat'],
                                          size=self.detector.input_shape, overlap=subscale['overlap'])

            window_images = []
            for frame_window, target_window in zip(frame_windows, target_windows):
                window_image = target_image[target_window[0][1]:target_window[1][1],
                               target_window[0][0]:target_window[1][0]]
                window_images.append(window_image)
            window_predictions = self.detector.predict(window_images)
            car_windows.extend(itertools.compress(frame_windows, window_predictions))

        return car_windows

    def visualize(self, road_image: np.array, car_windows: list, color=(0, 255, 0), thickness=2):
        for window in car_windows:
            cv2.rectangle(road_image, pt1=window[0], pt2=window[1], color=color, thickness=thickness)
        return road_image
