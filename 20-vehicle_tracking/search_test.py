import os
import cv2
import unittest
import itertools

from search import VehicleSearch


class VehicleSearchTest(unittest.TestCase):
    tests_dir = './tests/'
    results_dir = './results/search/'

    def test_visualization(self):
        search = VehicleSearch()
        input = cv2.imread(self.tests_dir + 'test4.jpg')
        input = cv2.cvtColor(input, code=cv2.COLOR_BGR2RGB)
        for subscale, color in zip(search.subscales, itertools.product([0, 255], repeat=3)):
            search.visualize(input, search.windows(**subscale), color=color, thickness=1)
        result = cv2.cvtColor(input, code=cv2.COLOR_RGB2BGR)
        cv2.imwrite(self.results_dir + 'windows.jpg', result)

    def test_search(self):
        search = VehicleSearch()
        for image_file in filter(lambda img: img.endswith('.jpg'), os.listdir(self.tests_dir)):
            input = cv2.imread(self.tests_dir + image_file)
            input = cv2.cvtColor(input, code=cv2.COLOR_BGR2RGB)
            on_windows = search.search(input)
            result = search.visualize(input, on_windows)
            result = cv2.cvtColor(result, code=cv2.COLOR_RGB2BGR)
            cv2.imwrite(self.results_dir + image_file, result)
