
## Lesson 18 - Vehicle Detection & Tracking

<img src="vehicle_tracking.jpg" width="50%" alt="Vehicle Tracking" />



### Submitted Files & Code

Here's an overview of the content included in the project archive:
 - `readme.md`: Write-up that summarizes the project.
 - `detection.py`: Code to classify if there's car in an image.
 - `detection_test.py`: Optimization runs and tests for the above.
 - `search.py`: Code to search for vehicles in an image.
 - `search_test.py`: Tests for the above.
 - `track.py`: Code to detect and track vehicles in vides.
 - `track_test.py`: Tests for the above.
 - `dataset/*`: Dataset used to train the `VehicleDetector` class.
 - `tests/*`: Input images and videos used for testing the algorithm.
 - `results/*`: Output images and videos as processed by the algorithm.

I did my best to write short, concise and readable functions, but I didn't have time to include
the documentation for each of them. I'm really sorry for that, but the end of the term is quickly
approaching. Each of the pieces of detection -> search -> tracking come with it's own test suite
that makes sure everything works as expected and also produces the output images and videos in
the `results/` folder.

If you want to run all the test it needs to be done in the correct sequence since the trained
classifier is pickled and loaded later. Some of the test are large scale searches over a grid
of possible parameters and are designed to be run over night. Producing the final video takes
about 50 minutes.

Here's how to run all of the tests and training:
```sh
python detection.py
python -m unittest detection_test.py
python -m unittest search_test.py
python -m unittest track_test.py
```



### Goals

* Perform a Histogram of Oriented Gradients (HOG) feature extraction on a labeled training set of
  images and train a Linear SVM classifier
* Optionally, you can also apply a color transform and append binned color features, as well as
  histograms of color, to your HOG feature vector.
* Implement a sliding-window technique and use your trained classifier to search for vehicles in
  images.
* Run your pipeline on a video stream (start with the `test_video.mp4` and later implement on full
  `project_video.mp4`) and create a heat map of recurring detections frame by frame to reject
  outliers and follow detected vehicles.
* Estimate a bounding box for each detected vehicles.



### Feature Extraction

All the components of my feature extraction and classification pipeline are written as subclasses
of `BaseEstimator` or `TransformerMixin`, so it's possible to piece them together like LEGO bricks
using the `Pipeline` class from `sklearn`. All the code described in this section is placed in the
`detect.py` file. The unit test class `VehicleDetectTest` is in the `detect_test.py` file.

The following image shows the entire pipeline for easier reference. Each block is described in more
detail later:

<img src="results/classifier.png" width="40%" alt="Classifier Pipeline" />

The search for the best features is performed by `GridSearchCV` class from `sklearn`. This class
allows effective parallel runs of different configuration on machines with multiple CPU cores.
The optimization searches were well worth the time spent on them as the performance (especially
false positives) of the classifier is critical for success of the later stages of the entire
vehicle tracking algorithm.

Optimization of each of the 3 branches also searched for the best colorspace where to take the
features from. Unsurprisingly, the most useful is the V channel from HSV colorspace. The search
space iterates over each channel from RGB, HSV and YUV colorspaces. I decided to use the F1-Score
as the criteria for my optimization runs. The reason is that both accuracy and recall matter in
this scenario. We want to both detect vehicles and to avoid false positives which cause trouble
in the tracking pipeline.

I used the standard dataset provided by Udacity for this project with a small augmentation. Each
image is vertically flipped and added to the dataset which effectively doubles its size. Interesting
thing to note is that while the training dataset is balanced the testing (i.e. road images & sliding
window) is very unbalanced as most of the images space is actually car-free.

To speed things up the dataset is loaded only once and then stored into a pickle file. The code
for this is in the `VehicleDetect.load_dataset()` function.


#### Color Channel Histogram
The code for this step is in the class `HistogramFeatureExtractor`.  I explored different color
spaces and different numbers of bins to figure out what is the best indicator of a vehicle. The code
that runs the optimization is in the `VehicleDetectTest.test_histogram_optimize()` function. The
result of the optimization is returned by the `VehicleDetectTest.optimal_histogram()`.

Obviously using more features will almost always improve performance of the classifier, so in order
to keep the number of features reasonable one has to decide on the best trade-off. The following
plot shows how accuracy a `LinearSVC` performs:

<img src="results/histogram-tradeoff.png" width="50%" alt="Color Histogram Tradeoff" />

The best feature seems to be 64 bins and B channel from the RGB colorspace. After ~60 bins the
performance of the classifier is not imporving at all. The insight for the colorspace decision
here is that the B channel indicates the presence of the sky, which shouldn't have any cars.


#### Spatial Binning
The code for this step is in the class `SpatialFeatureExtractor`. I explored all the color spaces
and different sizes of the spatial binning to arrive at the best `VehicleDetectTest.optimal_spatial()`.
The code for the grid parameter search is in the `VehicleDetectTest.spatial_optimize()`.

The following plot shows the trade off curve between number of spatially binned features and the
F1-score:

<img src="results/spatial-tradeoff.png" width="50%" alt="Spatial Binning Tradeoff" />

It's clear that after about ~500 parameters the performance of the classifier is hardly improving
and it does not make sense to add more. The final parameters are 16x16 spatial bins and V channel
from the HSV colorspace.


#### Histogram of Oriented Gradients (HOG)
The code that extracts the HOG features from the image is in the `HogFeatureExtractor` class.

I explored different `skimage.hog()` parameters to achieve the best performance of the classifier.
The run over the search space is implemented in `test_hog_optimize()` from the `VehicleDetectTest` class.
Besides that, the search space also contains the usual sweep over each channel of the usual
colorspaces. The optimized pipeline is the output by the `optimal_hog()` function.

The following plot shows a slightly more complicated trade-off between the F1-Score and number of
extracted HOG features:

<img src="results/hog-tradeoff.png" width="50%" alt="HOG Tradeoff" />

It's clear that there's not a single right answer here, and running classifier with 10s of thousands
of features may not be the best idea since the plan is to use a sliding window over a much larger
image. I decided to use `orientations=12`, `cells_per_block=4` and `pixels_per_cell=8`. The optimal
input for the `HogFeatureExtractor` is V channel from HSV colorspace.


#### Final Classifier Pipeline
I run the optimizations for each of the three branches and then put the optimized feature extractors
together to form the final pipeline using the `FeatureUnion` class. Obviously this is not equivalent
to running the large scale optimization for all the parameters in the entire pipeline, but hopefully
these are close enough for practical purposes. The number of searched parameters search makes this
practically infeasible with my resources (Intel i7 CPU/8 cores).

The following image shows the extracted features for a typical car image:

<img src="results/features.png" width="80%" alt="Extracted Features" />

The code that builds the final pipeline is in the constructor of the `VehicleDetect` class. This
class can also save/load itself to/from a pickle file using the static `load()`/`save()` methods.

The final classifier uses SVM with a Gaussian kernel and the search for optimal `C` and `gamma` is
done in `VehicleDetectorTest.test_detector_optimize()`. The following chart shows the 1-precision
vs 1-recall curves for each of the optimized sub-classifiers described above and also the final
classifier:

<img src="results/precision-recall.png" width="70%" alt="Precision-Recall Curve" />

The final classifier performs about two orders of magnitude better and achieves 99.6% training
F1-score when trained with the entire dataset. Large chunk of the improvement comes from using
non-linear SVM classifier.



### Sliding Window Search

The code that searches for vehicles in an image is concentrated in the `search.py` file and
`VehicleSearch` class.

I played around with different scales but finally decided to use only one. I use 100x100 windows
with 90% overlap. Generally bigger overlaps seem to work better. This setup is probably not optimal
but adding more subscales doesn't improve the false positives neither doesn't contribute to better
localization of detected vehicles. Here's a visualization of the search grid:

<img src="results/search/windows.jpg" width="60%" alt="Sliding Window Visualization" />

The code that performs the search is in `VehicleSearch.search()`. The `windows()` function returns
the windows to be searched in a given image. Here's an example result of the sliding window search
on a typical image:

<img src="results/search/test4.jpg" width="60%" alt="Example of Vehicle Search" />

More images can be found in the `results/search/` folder.

Notably, most images contain some obvious false positives. There's about 2,000 classification
performed in each frame. Only about 5% of the surface are actual cars, so even with 99.6% accuracy
one can expect about 8 false positives for each frame and that's why it makes sense to spend so
much time on the parameter optimization.



### Vehicle Tracking

The tracking uses a relaxed heatmap. For each image for every detected window with a car +1 is added
to the heatmap. The code for `VehicleTrack` and `VehicleTrackTest` are in `track.py` and
`track_test.py` respectively.

After adding new windows to the heatmap I do absolute and relative tresholding. Each point in the
heatmap must be above absolute and relative threhsold. I hand tuned these parameters to about
10 and 40% of the maximal heatmap value.

To detect the cars in thresholded heatmap I use `scipy.ndimage.measurements.label()` that numbers the
isolated "islands" on the heatmap that form each car. Then I use the `scipy.ndimage.measurements.find_objects()`
to get a bounding box for each of the "islands".

Before each step the heatmap is relaxed to 50% of it's value to forgot the old result, but to still
keep some significance of the previous detection. This parameter was also professionally eyeballed
as the thresholds :) Here's an example output from the tracking algorithm (more examples are in the
`results/track/` folder):

<img src="results/track/test6.jpg" width="60%" alt="Example of Vehicle Tracking" />

The heatmap is overlaid as a white layer on top of the image. For debugging I also overlaid the
heatmap on the final video `results/track/project_video.mp4`.

The pipeline performs reasonably well, there are some short occurances of false positives near the
beginning and to be hones I'm not sure what to do with these. The false positives are fairly
consistent - they hit about 10 sliding windows in several consecutive frames. One way to fix these
may be to add more road texture images into the detector training dataset.



### Discussion

I assume it would be very difficult to make this algorithm run in real-time. Currently, the pipeline
is super slow. It takes about 1.5s to process a single image. To achive real time performance one
would need rougly a 20x speed-up.

One of the bottlenecks is the Gaussian SVM. The profiling shows that about about 40% of the time
is spent there. 35% is spent on extracting the HOG features. Manipulation of the heatmap takes
about 10%.

One optimization would be to run the sliding windows in sync with HOG's `pixels_per_block` this way
the HOG features can be calculated in bulk and reused by the following window instead of doing a
new calcultion on an input that's overlapping by 90%.

Another optimization would be to reduce the scale of the heatmap. It doesn't really make sense to
have the scale of the image, because the maximal resolution it can have is the overlap step. This
phenomena is visible from the obvious pixelization of the "white heatmap confidence fog" overlaid
on the final video.

The detector fails at distinguish between 2 separate vehicles and treats them as a single detection
when they are too close or occlude each other. I'm not sure how to reliably fix this approach.
Segmentation by color may help, but not for two vehicles of the same color.
