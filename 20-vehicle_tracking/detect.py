import cv2
import glob
import pickle
import numpy as np
import skimage.feature

from sklearn.svm import SVC
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.metrics import f1_score


class EstimatorParamGetSet:
    def __init__(self):
        import inspect
        self.init_signature = inspect.signature(self.__init__)

    def set_params(self, **params):
        for param_name, param_value in params.items():
            if param_name in self.init_signature.parameters:
                setattr(self, param_name, param_value)
        return self

    def get_params(self, deep=False):
        params = {}
        for parameter_name in self.init_signature.parameters:
            params[parameter_name] = getattr(self, parameter_name)
        return params

    def fit(self, x, y=None):
        return self


class ColorChannelExtractor(TransformerMixin, EstimatorParamGetSet):
    def __init__(self, space: str='HSV', channel: int=2):
        EstimatorParamGetSet.__init__(self, )
        self.space = space
        self.channel = channel

    def transform(self, rgb_images: np.array) -> np.array:
        if self.space != 'RGB':
            code = getattr(cv2, 'COLOR_RGB2' + self.space)
        channel_list = []
        for rgb_image in rgb_images:
            if self.space != 'RGB':
                trans_image = cv2.cvtColor(rgb_image, code=code)
            else:
                trans_image = rgb_image
            channel_list.append(trans_image[:, :, [self.channel]])
        return np.array(channel_list)

    def visualize(self, rgb_image: np.array) -> np.array:
        if self.space != 'RGB':
            code = getattr(cv2, 'COLOR_RGB2' + self.space)
            trans_image = cv2.cvtColor(rgb_image, code=code)[:, :, [self.channel]]
        else:
            trans_image = rgb_image
        return trans_image[:, :, [self.channel]]


class SpatialFeatureExtractor(TransformerMixin, EstimatorParamGetSet):
    def __init__(self, dsize: tuple=(16, 16)):
        EstimatorParamGetSet.__init__(self)
        self.dsize = dsize

    def transform(self, gray_images: np.array) -> np.array:
        spatial_list = []
        for gray_image in gray_images:
            spatial = cv2.resize(gray_image.squeeze(), dsize=self.dsize, interpolation=cv2.INTER_LINEAR)
            spatial_list.append(spatial.ravel())
        return np.array(spatial_list, dtype=np.float32)

    def visualize(self, gray_image: np.array) -> np.array:
        spatial = cv2.resize(gray_image.squeeze(), dsize=self.dsize, interpolation=cv2.INTER_LINEAR)
        return spatial


class HistogramFeatureExtractor(TransformerMixin, EstimatorParamGetSet):
    def __init__(self, bins: int=16, range: tuple=(0, 255)):
        EstimatorParamGetSet.__init__(self)
        self.bins = bins
        self.range = range

    def transform(self, gray_images: np.array) -> np.array:
        histogram_list = []
        for gray_image in gray_images:
            histogram, bins = np.histogram(gray_image.squeeze(), bins=self.bins, range=self.range)
            histogram_list.append(histogram)
        return np.array(histogram_list, dtype=np.float32)

    def visualize(self, gray_image: np.array) -> np.array:
        histogram, bins = np.histogram(gray_image.squeeze(), bins=self.bins, range=self.range)
        return histogram


class HogFeatureExtractor(TransformerMixin, EstimatorParamGetSet):
    def __init__(self, orientations: int=9, pixels_per_cell: tuple=(8, 8),
                 cells_per_block: tuple=(2, 2)):
        EstimatorParamGetSet.__init__(self)
        self.orientations = orientations
        self.pixels_per_cell = pixels_per_cell
        self.cells_per_block = cells_per_block

    def transform(self, gray_images: np.array) -> np.array:
        hog_list = []
        for gray_image in gray_images:
            hog = skimage.feature.hog(gray_image.squeeze(), orientations=self.orientations,
                                      pixels_per_cell=self.pixels_per_cell,
                                      cells_per_block=self.cells_per_block,
                                      transform_sqrt=True, visualise=False, feature_vector=True)
            hog_list.append(hog)
        return np.array(hog_list, dtype=np.float32)

    def visualize(self, gray_image: np.array) -> np.array:
        hog, hog_vis = skimage.feature.hog(gray_image.squeeze(), orientations=self.orientations,
                                           pixels_per_cell=self.pixels_per_cell,
                                           cells_per_block=self.cells_per_block,
                                           transform_sqrt=True, visualise=True, feature_vector=True)
        return hog_vis


class VehicleDetect(BaseEstimator, EstimatorParamGetSet):
    def __init__(self):
        spatial_steps = [('color', ColorChannelExtractor(space='HSV', channel=2)),
                         ('spatial', SpatialFeatureExtractor(dsize=(16, 16)))]
        spatial_pipeline = Pipeline(steps=spatial_steps)
        histogram_steps = [('color', ColorChannelExtractor(space='RGB', channel=2)),
                           ('histogram', HistogramFeatureExtractor(bins=64))]
        histogram_pipeline = Pipeline(steps=histogram_steps)
        hog_steps = [('color', ColorChannelExtractor(space='HSV', channel=2)),
                     ('hog', HogFeatureExtractor(orientations=12, cells_per_block=(4, 4), pixels_per_cell=(8, 8)))]
        hog_pipeline = Pipeline(steps=hog_steps)

        union_features = [('spatial_pipe', spatial_pipeline),
                          ('histogram_pipe', histogram_pipeline),
                          ('hog_pipe', hog_pipeline)]
        detector_steps = [('union', FeatureUnion(union_features, n_jobs=-1)),
                          ('scale', StandardScaler()),
                          ('svm', SVC(C=10, gamma=0.001))]
        self.pipeline = Pipeline(steps=detector_steps)
        self.input_shape = (64, 64)

    @staticmethod
    def save(classifier, filename: str='./results/detector.pkl'):
        with open(filename, 'wb') as file:
            data = {'classifier': classifier}
            pickle.dump(data, file, protocol=0)

    @staticmethod
    def load(filename: str='./results/detector.pkl'):
        with open(filename, 'rb') as file:
            data = pickle.load(file)
            return data['classifier']

    @staticmethod
    def load_dataset(dataset_filename: str='./dataset/dataset.pkl') -> (np.array, np.array):
        try:
            with open(dataset_filename, 'rb') as file:
                dataset = pickle.load(file)
                features, labels = dataset['features'], dataset['labels']
        except FileNotFoundError:
            vehicles = []
            non_vehicles = []
            for image_filename in glob.glob('./dataset/*/*/*.png', recursive=True):
                image = cv2.imread(image_filename)
                image = cv2.cvtColor(image, code=cv2.COLOR_BGR2RGB)
                flip = cv2.flip(image, flipCode=1)
                if 'non-vehicles' in image_filename:
                    non_vehicles.append(image)
                    non_vehicles.append(flip)
                else:
                    vehicles.append(image)
                    vehicles.append(flip)

            feature_vehicles = np.array(vehicles)
            feature_non_vehicles = np.array(non_vehicles)
            label_vehicles = np.ones(shape=[len(vehicles)], dtype=np.uint8)
            label_non_vehicles = np.zeros(shape=[len(non_vehicles)], dtype=np.uint8)

            features = np.concatenate([feature_vehicles, feature_non_vehicles])
            labels = np.concatenate([label_vehicles, label_non_vehicles])
            with open(dataset_filename, 'wb') as file:
                dataset = {'features': features, 'labels': labels}
                pickle.dump(dataset, file)
        return features, labels

    def fit(self, x, y=None):
        return self.pipeline.fit(x, y)

    def transform(self, x):
        return self.pipeline.transform(self)

    def predict(self, x):
        return self.pipeline.predict(x)


if __name__ == '__main__':
    from detect import VehicleDetect
    features, labels = VehicleDetect.load_dataset()
    permutation = np.random.permutation(features.shape[0])
    features, labels = features[permutation], labels[permutation]

    print("Training VehicleDetect on the entire dataset...")
    vehicle_detect = VehicleDetect()
    vehicle_detect.fit(features, labels)
    VehicleDetect.save(vehicle_detect)
    f1score = f1_score(labels, vehicle_detect.predict(features))
    print("F1-Score: {}".format(f1score))
