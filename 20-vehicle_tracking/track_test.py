import os
import cv2
import unittest
import moviepy.editor as mpy

from track import VehicleTrack


class VehicleTrackTest(unittest.TestCase):
    tests_dir = './tests/'
    results_dir = './results/track/'

    def test_visualization(self):
        track = VehicleTrack()
        for image_file in filter(lambda img: img.endswith('.jpg'), os.listdir(self.tests_dir)):
            input = cv2.imread(self.tests_dir + image_file)
            input = cv2.cvtColor(input, code=cv2.COLOR_BGR2RGB)
            result = track.track(input)
            result = cv2.cvtColor(result, code=cv2.COLOR_RGB2BGR)
            cv2.imwrite(self.results_dir + image_file, result)
            track.reset()

    def test_track(self):
        track = VehicleTrack()
        for video_file in filter(lambda mov: mov.endswith('.mp4'), os.listdir(self.tests_dir)):
            input = mpy.VideoFileClip(self.tests_dir + video_file)
            result = input.fl_image(track.track)
            result.preview(audio=False)
            result.write_videofile(self.results_dir + video_file, codec='mpeg4', audio=False)
            track.reset()
