
## Lesson 14 - Behavioral Cloning

<img src="behavioral_cloning.png" width="50%" alt="Simulator" />



### Submitted Files & Code

Here's an overview of the content included in the project archive:
 - `readme.md`: Write-up that summarizes the results.
 - `preprocessor.py`: Pre-processing and data loading for the model.
 - `model.py`: Code to create and train the model
 - `model.h5`: Weights of the final trained network.
 - `drive.py`: Script for autonomous driving.
 - `results/*`: Schematics and videos output by the model when driving.

The `model.py` file contains the code for training and saving the neural network. `preprocess.py`
is the pipeline class that is used load, organize, augment and pre-process the training dataset.
I did my best to write short, concise and readable functions. `Preprocessor` class was originally
a generator for larger images, but since my scaled-down _16x32_ dataset can easily fit into memory
of any recent computer it's no longer needed. Each file comes with it's own set of test cases that
makes sure everything is working correctly and also outputs graphs and charts of tested results.

Using the Udacity simulator and my `drive.py` file, the car can be driven autonomously around
both tracks by executing the following command:

```sh
# Train the model and drive it on the testing dataset.
python -m unittest model.py
# Drive in autonomous mode.
python drive.py model.h5
```

Unfortunately, I found out that the model is sensitive to the quality settings of the simulator.
Theoretically this shouldn't matter because the image is scaled to _16x32_ before being fed into
the network, but for some settings and some resolutions the model doesn't complete the second
track. The problem generally gets worse with higher quality and higher resolution, so I assume
it is related to the latency of my computer and since rendering each frame takes longer there's
fewer steering angle updates each second which leads to bigger _"wobbling"_ and eventual crash.

I didn't have time do any profiling or real-time factor analysis to prove my assumptions. To
makes thing simpler I used the lowest setting and _640x480_ resolution for all of my development.



### Dataset, Preprocessing & Model Training Strategy

Initially, I used the center camera images of the un-modified original Udacity dataset with a
little bit of data augmentation. However, the testing track is a closed loop there's more data
turning in one direction than the other. This inherent imbalance leads the model to bias towards
one direction. To overcome the preference to steer to one side I've added a left-right flipped
copy of each image into the dataset. The steering angle was replaced with it's own negative
value to keep everything consistent.

This resolved the biased steering problem, but the dataset didn't contain enough data that
displayed recovery from the off-center position on the track. The vehicle had difficulty to take
sharper turns and to return back to the center after veering to a side. To mitigate this I've added
the left and right camera images from the Udacity dataset with a slightly offset steering angle.
The offset and the side camera position very well modelled return back to center of the road.

I tried to figure out the left and right camera position and angle from the
Simulator source code on [GitHub](https://github.com/udacity/self-driving-car-sim), but without
luck. I didn't want to dig into a rabbit hole of installing the _Unity 3D_ engine and all the
dependencies, so I trial-and-errored the value to reduce the _"drunk"_ (wobbly) driving in the
simulator. Interestingly, neither training loss nor validation loss values weren't particularly
sensitive to the choice of this value. The best offset values seems to be about _0.4_.

The following chart shows the distribution of the steering angles in the enhanced dataset. There
are three clearly visible peaks at _0.0_ and _+/-0.4_ Most of the time the car is simply driving
straight so the natural steering angle of _0.0_ gets _"offset"_ & _"copied"_ over with the left
and right camera images. Noteworthy fact is that the histogram is symmetric when flipped around
the vertical axis.

<img src="results/histogram.png" width="30%" alt="Steering Angles Histogram" />

The dataset provided by Udacity captured reasonably good driving behavior and I didn't need to
record any more data. Below is the overall diagram of the entire data augmentation process:

<img src="results/augmentation.png" width="50%" alt="Training Data Augmentation" />

After augmentation, the dataset consists of about _48,000_ images. Prior to being stored in memory
each image went through a handful of simple pre-processing steps. The pipeline is displayed on the
following image:

<img src="results/preprocessing.png" width="100%" alt="Input Preprocessing" />

The original _160x320x3_ RGB image is scaled down to _16x32x3_ and then converted to HSV
color-space. After some trial and error I found out that the S channel (saturation) provides the
best contrast between the road and the environment. Also, the entire dataset was randomly shuffled
after each epoch and 10% of images was put into a validation set. The validation set helped
determine if the model was over- or under-fitting.

During my work on the project I discovered that low validation MSE (Mean Squared Error) didn't
mean that this particular model would perform well in the simulator. I couldn't come up with
a better performance metric, so I used with RMS anyway. Because of this I didn't want to use
too much data for validation and put too much emphasis on it. The ideal number of epochs to
train the model is about _30_-_40_ as evidenced by the following plot of training and validation
loss:

<img src="results/training.png" width="30%" alt="Input Preprocessing" />

I used the _ADAM_ optimizer and therefore no manual tuning of hyper-parameters was necessary.



### Model Architecture

My model consists of a simple convolutional neural network. There's a single `Convolution2D`
layer with _3x3_ filter and depth of _8_. The next layer is `ReLU` activation followed by
`MaxPooling2D` with _3x3_ window and _3x3_ stride. After that the values are `Flatten`ed,
go through `Dropout` and finally enter the last fully-connected `Dense` layer. Output of
the very last layer is used directly as a steering angle for the simulator.

The following schematics shows the network architecture:

<img src="results/network.png" width="100%" alt="Convolutional Neural Network Architecture" />

The overall strategy for deriving this model architecture was to start with simple things and
improve them in the areas where they don't work. I wanted to avoid over-designing my network and
consequently spend $$$ on AWS GPU instances. The final network is so small that it can be
trained locally on my _iMac_ CPU in a few minutes.

My initial idea was to use the approach described in the
['89 _ALVINN_ paper](http://www.dtic.mil/dtic/tr/fulltext/u2/a218975.pdf). I designed
fully-connected network with a single hidden layer with about _30_ neurons. The input to the
network was a scaled down (_16x32_) normalized gray-scale image. The _ALVINN_ paper uses B (blue)
channel color from RGB as input. This didn't provide enough contrast between the road and other
objects in the simulator so I switched first keeping the V-channel from HSV converted image and
later found out that the S-channel provides better road vs environment contrast.

Compared to the _ALVINN_ net, I didn't use classifier-style like one-hot encoded output
consequently converted to a steering angle value, but instead utilized a single output neuron.
Compared to _ALVINN_ architecture this variation wasn't able to provide information about the
uncertainty of its prediction, but arguably this isn't as important in the simulator.

The _ALVINN_ inspired fully connected network performed well on the training track, but failed on
the darker second track. To make the model generalize better I decided to add a convolutional
layer. Fully connected networks don't take full advantage of close spatial correlation between
the individual input pixels, so convolution should perform very well on this problem.

Alternatively, this architecture can be viewed as a middle section cut-out from _LeNet_, at the
point where the convolutional layers connect to the fully-connected classifier layers. I have
arrived this architecture by manual trial-and-error runs. I mainly changed the shape of the
convolutional filter and max-pooling kernels and strides. Given the _14x32_ input size, _3x3_
seems to be the largest value that is still able to capture and train filters that capture
the turns and steer. Larger filter values were essentially  tend to simply converge onto the
_"average"_ steering value of the dataset and permanently output zero value with tiny
variation.

To see how the convolutional I did a visualization of the weights of each of the _8_ filters
present in the model. The following image shows the filter weights (_positive=red_,
_negative=blue_):

<img src="results/convolution-weights.png" width="45%" alt="Convolutional Layer Weights" />

I also visualized how well the convolutional layer worked by plotting output from the
`Convolution2D`, `ReLU` activation, `MaxPool2D` and a _"maximization"_ filter input image. The
following chart shows each of these for every filter in the top-down manner (_positive=red_,
_negative=blue_ and _gray=always-positive_):

<img src="results/convolution-output.png" width="50%" alt="Convolutional Layer Outputs" />

The _"maximize"_ filter image was obtained be optimizing the average output value for that
particular filter. By calculating the derivative of this objective with respect to the input
image I was able to obtain the gradient. The gradient is in fact back-propagated error that
would make this filter output higher values, so by doing gradient descent on the random input
image, it is possible to arrive at an image that _"maximizes"_ the _"firing"_ of every filter.

In a similar fashion as the _"maximize"_ filter image, it is possible to do a back-propagated
gradient descent of the left and right turn objective to arrive at some kind of an _"ideal"_
image of what the network considers left and right turn. This is the result:

<img src="results/turns.png" width="45%" alt="Optimal Left and Right Turns" />

Another useful way of figuring what part of the image makes the network "fire" is to use
a technique called salination which back-propagates average filter outputs and deconvolutes
them in similar fashion as an autoencoder. The result looks like this:

<img src="results/salination.jpg" width="65%" alt="Salination" />

The code that performs these visualizations in `model.py` : `TestModel::test_2_visualize()` was
inspired by [this post on the _Keras_ blog](https://blog.keras.io/how-convolutional-neural-networks-see-the-world.html)
and by [this paper by nVidia](https://arxiv.org/abs/1704.07911).

To combat the over-fitting and to train more robust features, I modified the model to include a
`Dropout` layer after the convolution. As a last fine-tuning I added the `Cropping2D` layer that
removes the top _2px_ of the _16x32_ input. The reason is that the sky has very little relevance
to the steering angle and _14x32_ input size makes the following convolutional and max-pooling
layer kernels fit very neatly inside of the input without any overlaps.

The final network has _401_ parameters of which about _80%_ belongs to the final fully-connected
layer and the remaining _20%_ to the convolutional filters.

The following image shows the ground truth and predicted steering angle output by the model on a
training dataset lap:

<img src="results/prediction.png" width="80%" alt="Prediction on the Training Dataset" />

The final step for each version of the network step was to run the simulator to see how well the
car was driving around both tracks. The second track is much more hilly and setting a constant
throttle value resulted in very high speeds on downhill sections and slow speeds when going up a
hill. To prevent this I wrote a simple PID controller that sets the speed to _20 MPH_ and actuates
the throttle to maintain it. I used the lowest _640x480_ resolution and the worst quality.
