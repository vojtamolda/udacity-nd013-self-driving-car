import os
import cv2
import unittest
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

from keras import backend
from keras.engine.topology import Layer
from keras.initializers import Zeros, Ones
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation, Flatten, Dropout, Conv2D, MaxPooling2D, Cropping2D, UpSampling2D, Conv2DTranspose


from preprocessor import Preprocessor


def convolutional() -> Sequential:
    model = Sequential()
    model.add(Cropping2D(name='input', cropping=[(0, 2), (0, 0)], input_shape=Preprocessor.shape[1:]))
    model.add(Conv2D(kernel_size=(3, 3), filters=8, name="conv", padding="valid"))
    model.add(Activation(name='relu', activation='relu'))
    model.add(MaxPooling2D(padding="valid", strides=(3, 3), pool_size=(3, 3), name="maxpool"))
    model.add(Flatten(name='flatten'))
    model.add(Dropout(rate=0.75, name="dropout"))
    model.add(Dense(units=1, name="output"))
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


class FeatureAverage2D(Layer):
    def __init__(self, **kwargs):
        super(FeatureAverage2D, self).__init__(**kwargs)

    def build(self, input_shape):
        pass

    def call(self, x):
        return backend.mean(x, axis=3, keepdims=True)

    def compute_output_shape(self, input_shape):
        return input_shape[:3] + (1,)


class TestModel(unittest.TestCase):

    def test_1_training(self) -> None:
        preproc = Preprocessor(batch_size=2048)
        model = convolutional()

        model.summary()
        images, steer_angles = preproc.load_dataset(preproc.files)
        history = model.fit(images, steer_angles, batch_size=2048, epochs=40, validation_split=0.1)
        model.save('model.h5')

        plt.figure()
        plt.title("Training")
        plt.hold(True)
        plt.plot(history.history['loss'], 'r')
        plt.plot(history.history['val_loss'], 'y')
        plt.legend({"Training Loss", "Validation Loss"})
        plt.hold(False)
        plt.show(block=True)

    def test_2_visualize(self) -> None:
        rows, columns = 2, 4
        preprocessed = Preprocessor.open('center_2016_12_01_13_33_05_701.jpg', original=False, normalize=True)
        model = load_model('model.h5')
        layers = {layer.name: layer for layer in model.layers}
        input_layer = layers['input']
        convolute_layer = layers['conv']
        maxpool_layer = layers['maxpool']
        output_layer = layers['output']

        plt.figure()
        plt.title('Back-Propagated Left/Right Turn')
        for idx, steer, name in zip(range(2), [-1.0, +1.0], ['Left Turn [-]', 'Right Turn [+]']):
            subplot = plt.subplot(1, 2, idx + 1)
            subplot.set_title("{}".format(name))
            subplot.axes.get_xaxis().set_visible(False)
            subplot.axes.get_yaxis().set_visible(False)

            criteria = backend.mean(output_layer.output)
            gradients = backend.gradients(criteria, input_layer.input)[0]
            gradients /= backend.sqrt(backend.mean(backend.square(gradients)))
            black_magic_function = backend.function([input_layer.input, backend.learning_phase()], [criteria, gradients])
            image = np.random.random((1,) + input_layer.input_shape[1:])
            for i in range(90):
                criteria_value, gradients_value = black_magic_function([image, 0])
                image = image + steer * gradients_value
            plt.imshow(image.squeeze(), cmap='gray_r', interpolation='nearest')
        plt.show(block=True)

        plt.figure()
        plt.title('Convolutional Layer Weights')
        weights, biases = convolute_layer.get_weights()
        vmin, vmax = weights.min(), weights.max()
        vmin, vmax = min(-vmax, vmin), max(-vmin, vmax)
        for row in range(rows):
            for column in range(columns):
                idx = row * columns + (column + 1)
                subplot = plt.subplot(rows, columns, idx)
                subplot.set_title("{}".format(idx))
                subplot.axes.get_xaxis().set_visible(False)
                subplot.axes.get_yaxis().set_visible(False)
                plt.imshow(weights[:, :, :, idx - 1].squeeze(), vmin=vmin, vmax=vmax, cmap='coolwarm', interpolation='nearest')
        plt.show(block=False)

        plt.figure()
        plt.title('Convolutional Layer Output')
        black_magic_function = backend.function([input_layer.input, backend.learning_phase()], [convolute_layer.output])
        conv_output = black_magic_function([np.array([preprocessed]), 0])[0]
        vmin, vmax = conv_output.min(), conv_output.max()
        vmin, vmax = min(-vmax, vmin), max(-vmin, vmax)
        for row in range(rows):
            for column in range(columns):
                idx = row * columns + (column + 1)
                subplot = plt.subplot(rows, columns, idx)
                subplot.set_title("{}".format(idx))
                subplot.axes.get_xaxis().set_visible(False)
                subplot.axes.get_yaxis().set_visible(False)
                plt.imshow(conv_output[:, :, :, idx - 1].squeeze(), vmin=vmin, vmax=vmax, cmap='coolwarm', interpolation='nearest')
        plt.show(block=False)

        plt.figure()
        plt.title('Max-Pooling Layer Output')
        black_magic_function = backend.function([input_layer.input, backend.learning_phase()], [maxpool_layer.output])
        maxp_output = black_magic_function([np.array([preprocessed]), 0])[0]
        vmin, vmax = maxp_output.min(), maxp_output.max()
        vmin, vmax = min(-vmax, vmin), max(-vmin, vmax)
        for row in range(rows):
            for column in range(columns):
                idx = row * columns + (column + 1)
                subplot = plt.subplot(rows, columns, idx)
                subplot.set_title("{}".format(idx))
                subplot.axes.get_xaxis().set_visible(False)
                subplot.axes.get_yaxis().set_visible(False)
                plt.imshow(maxp_output[:, :, :, idx - 1].squeeze(), vmin=vmin, vmax=vmax, cmap='gray_r', interpolation='nearest')
        plt.show(block=False)

        plt.figure()
        plt.title('Back-Propagated Maximum Filter Activation')
        for row in range(rows):
            for column in range(columns):
                idx = row * columns + (column + 1)
                subplot = plt.subplot(rows, columns, idx)
                subplot.set_title("{}".format(idx))
                subplot.axes.get_xaxis().set_visible(False)
                subplot.axes.get_yaxis().set_visible(False)

                criteria = backend.mean(convolute_layer.output[:, :, :, idx-1])
                gradients = backend.gradients(criteria, input_layer.input)[0]
                gradients /= backend.sqrt(backend.mean(backend.square(gradients)))
                black_magic_function = backend.function([input_layer.input, backend.learning_phase()], [criteria, gradients])
                image = np.random.random((1,) + input_layer.input_shape[1:])
                for i in range(30):
                    criteria_value, gradients_value = black_magic_function([image, 0])
                    image += gradients_value
                plt.imshow(image.squeeze(), cmap='gray_r', interpolation='nearest')
        plt.show(block=False)

        plt.figure()
        plt.title('Back-Propagated Left/Right Turn')
        for idx, steer, name in zip(range(2), [-1.0, +1.0], ['Left Turn [-]', 'Right Turn [+]']):
            subplot = plt.subplot(1, 2, idx + 1)
            subplot.set_title("{}".format(name))
            subplot.axes.get_xaxis().set_visible(False)
            subplot.axes.get_yaxis().set_visible(False)

            criteria = backend.mean(output_layer.output)
            gradients = backend.gradients(criteria, input_layer.input)[0]
            gradients /= backend.sqrt(backend.mean(backend.square(gradients)))
            black_magic_function = backend.function([input_layer.input, backend.learning_phase()], [criteria, gradients])
            image = np.random.random((1,) + input_layer.input_shape[1:])
            for i in range(90):
                criteria_value, gradients_value = black_magic_function([image, 0])
                image = image + steer * gradients_value
            plt.imshow(image.squeeze(), cmap='gray_r', interpolation='nearest')
        plt.show(block=True)

    def test_3_prediction(self) -> None:
        preprocess = Preprocessor()
        model = load_model('model.h5')
        pred_angles, steer_angles = [], []

        plt.figure()
        plt.gca().get_xaxis().set_visible(False)
        plt.gca().axes.get_yaxis().set_visible(False)
        movie = plt.imshow(np.ones((1, 1, 3)))
        for idx, (image_file, steer_angle) in enumerate(preprocess.files.items()):
            if not plt.get_fignums():
                break
            plt.title("Training ({}/{})".format(idx, len(preprocess.files)))
            original = Preprocessor.open(image_file, original=True, normalize=False)
            preprocessed = Preprocessor.open(image_file, original=False, normalize=True)
            pred_angles.append(float(model.predict(np.array([preprocessed]), batch_size=1)[0]))
            steer_angles.append(steer_angle)
            preprocessed = Preprocessor.open(image_file, original=False, normalize=False)
            mixed = Preprocessor.postprocess(original, preprocessed, steer_angles[-1], pred_angles[-1])
            movie.set_array(mixed)
            plt.pause(0.01)

        plt.figure()
        plt.title("Predicted vs. Training Steering Angle")
        plt.hold(True)
        plt.plot(pred_angles)
        plt.plot(steer_angles)
        plt.hold(False)
        plt.legend({"Prediction", "Training"})
        plt.ylabel("Steering Angle [1]")
        plt.xlabel("Frame [1]")
        plt.show(block=True)

    def test_4_salinate(self) -> None:
        preprocess = Preprocessor()
        pred_angles, steer_angles = [], []
        model = load_model('model.h5')
        layers = {layer.name: layer for layer in model.layers}
        input_layer = layers['input']
        convolute_layer = layers['conv']
        maxpool_layer = layers['maxpool']

        salinate = Sequential()
        salinate.add(UpSampling2D(name='upsample', size=maxpool_layer.pool_size, input_shape=maxpool_layer.output_shape[1:]))
        salinate.add(FeatureAverage2D(name='average'))
        salinate.add(Conv2DTranspose(name='deconvolute', kernel_size=convolute_layer.kernel_size,
                                     filters=input_layer.output_shape[-1], padding=convolute_layer.padding,
                                     kernel_initializer=Ones(), bias_initializer=Zeros()))
        black_magic_function = backend.function([input_layer.input, backend.learning_phase()], [maxpool_layer.output])

        plt.figure()
        plt.gca().get_xaxis().set_visible(False)
        plt.gca().axes.get_yaxis().set_visible(False)
        movie = plt.imshow(np.ones((1, 1, 3)))
        for idx, (image_file, steer_angle) in enumerate(preprocess.files.items()):
            if not plt.get_fignums():
                break
            plt.title("Training ({}/{})".format(idx, len(preprocess.files)))
            original = Preprocessor.open(image_file, original=True, normalize=False)
            preprocessed = Preprocessor.open(image_file, original=False, normalize=True)
            pred_angles.append(float(model.predict(np.array([preprocessed]), batch_size=1)[0]))
            steer_angles.append(steer_angle)
            maxpool_output = black_magic_function([np.array([preprocessed]), 0])[0]
            salinated = salinate.predict(maxpool_output).squeeze()
            salinated = (255 * (salinated - salinated.min()) / (salinated.max() - salinated.min())).astype(np.uint8)
            salinated = np.dstack([np.zeros(shape=salinated.shape, dtype=np.uint8), salinated,
                                   np.zeros(shape=salinated.shape, dtype=np.uint8)])
            preprocessed = Preprocessor.open(image_file, original=False, normalize=False)
            preprocessed = preprocessed[:salinated.shape[0], :salinated.shape[1], :]
            preprocessed = cv2.cvtColor(preprocessed, code=cv2.COLOR_GRAY2RGB)
            blended = cv2.addWeighted(src1=preprocessed, alpha=1.00, src2=salinated, beta=0.25, gamma=1.00)
            mixed = Preprocessor.postprocess(original, blended, steer_angles[-1], pred_angles[-1])
            movie.set_array(mixed)
            timestamp = datetime.utcnow().strftime('%Y_%m_%d_%H_%M_%S_%f')[:-3]
            image_filename = os.path.join('results/', timestamp)
            cv2.imwrite('{}.jpg'.format(image_filename), cv2.cvtColor(mixed, code=cv2.COLOR_RGB2BGR))
            plt.pause(0.01)
