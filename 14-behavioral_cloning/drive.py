import os
import cv2
import flask
import shutil
import base64
import argparse
import socketio
import eventlet
import eventlet.wsgi
import numpy as np
from datetime import datetime
from keras.models import load_model

from preprocessor import Preprocessor


app = flask.Flask(__name__)
sio = socketio.Server()
model = None
pid = None


class PID:
    def __init__(self, target: float, kp: float = 0.5, ki: float = 0.0005):
        self.target = target
        self.kp, self.ki = kp, ki
        self.inte_error = 0

    def output(self, actual: float):
        error = self.target - actual
        inte_error = self.inte_error + error
        self.inte_error = inte_error

        retval = self.kp * error + self.ki * inte_error
        retval = min(+1.0, retval)
        retval = max(-1.0, retval)
        return retval


@sio.on('telemetry')
def telemetry(sid: str, data: dict) -> None:
    if data:
        buffer, speed = np.frombuffer(base64.b64decode(data['image']), dtype=np.uint8), float(data['speed'])
        original = cv2.imdecode(buffer, flags=cv2.IMREAD_COLOR)
        preprocessed = Preprocessor.preprocess(original, normalize=True)
        steering_angle, throttle = float(model.predict(np.array([preprocessed]), batch_size=1)[0]), pid.output(speed)
        print("{:+.2f} {:+.2f}".format(steering_angle, throttle))
        sio.emit('steer', data={'steering_angle': str(steering_angle), 'throttle': str(throttle)}, skip_sid=True)

        if args.image_folder != '':
            preprocessed = Preprocessor.preprocess(original, normalize=False)
            mixed = Preprocessor.postprocess(original, preprocessed, None, steering_angle)
            timestamp = datetime.utcnow().strftime('%Y_%m_%d_%H_%M_%S_%f')[:-3]
            image_filename = os.path.join(args.image_folder, timestamp)
            cv2.imwrite('{}.jpg'.format(image_filename), mixed)
    else:
        sio.emit('manual', data={}, skip_sid=True)


@sio.on('connect')
def connect(sid: str, environ: dict) -> None:
    print('connect ', sid)
    sio.emit('steer', data={'steering_angle': str(0), 'throttle': str(0)}, skip_sid=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Remote Driving')
    parser.add_argument('model', type=str,
                        help="Path to model h5 file. Model should be on the same path.")
    parser.add_argument('image_folder', type=str, nargs='?', default='',
                        help="Path to image folder. This is where the images from the run will be saved.")
    args = parser.parse_args()
    model = load_model(args.model)
    pid = PID(target=20)

    if args.image_folder != '':
        print("Creating image folder at {}".format(args.image_folder))
        if not os.path.exists(args.image_folder):
            os.makedirs(args.image_folder)
        else:
            shutil.rmtree(args.image_folder)
            os.makedirs(args.image_folder)
        print("RECORDING...")
    else:
        print("NOT RECORDING...")

    app = socketio.Middleware(sio, app)
    eventlet.wsgi.server(eventlet.listen(('', 4567)), app)
