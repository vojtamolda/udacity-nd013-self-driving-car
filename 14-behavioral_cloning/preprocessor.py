import csv
import cv2
import random
import unittest
import collections
import sklearn.utils
import numpy as np
import matplotlib.pyplot as plt


class Preprocessor:
    data_dir = 'data/img/'
    shape = [None, 16, 32, 1]
    correction = 0.40
    start = 0

    def __init__(self, batch_size: int = None) -> None:
        self.images, self.steer_angles = None, None
        self.files = self.load_csv()

        self.shape[0] = 2 * len(self.files)
        self.batch_size = batch_size if batch_size is not None else self.shape[0]

    def __iter__(self):
        return self

    def __next__(self) -> tuple:
        if self.images is None or self.steer_angles is None:
            self.images, self.steer_angles = self.load_dataset(self.files)
        if self.start >= self.shape[0]:
            self.image, self.steer_angles = sklearn.utils.shuffle(self.images, self.steer_angles)
            self.start = 0
        start, end = self.start, min(self.start + self.batch_size, self.shape[0])
        images, steer_angles = self.images[start:end], self.steer_angles[start:end]
        self.start = end
        return images, steer_angles

    def load_csv(self, csv_file: str = 'data/driving_log.csv') -> collections.OrderedDict:
        with open(csv_file, mode='r', encoding='utf8') as csv_file:
            files = collections.OrderedDict()
            for camera_position, steer_correction in zip(['center', 'left', 'right'], [0, +self.correction, -self.correction]):
                csv_reader = csv.DictReader(csv_file, delimiter=',')
                for row in csv_reader:
                    image_file, image_steer = row[camera_position], float(row['steering']) + steer_correction
                    files[image_file] = image_steer
                csv_file.seek(0)
            return files

    def load_dataset(self, files: dict) -> tuple:
        images = np.zeros(shape=self.shape, dtype=np.float32)
        steer_angles = np.zeros(shape=self.shape[0], dtype=np.float32)

        for idx, (file, steer_angle) in enumerate(files.items()):
            flip_idx = len(files) + idx
            image = self.open(file, original=False, normalize=True)
            images[idx], images[flip_idx] = image, cv2.flip(image, 1).reshape(self.shape[1:])
            steer_angles[idx], steer_angles[flip_idx] = steer_angle, -steer_angle
        return sklearn.utils.shuffle(images, steer_angles)

    @classmethod
    def open(cls, image_file: str, original: bool = False, normalize: bool = True) -> np.array:
        original_filename = cls.data_dir + image_file
        bgr = cv2.imread(original_filename)
        image = cv2.cvtColor(bgr, code=cv2.COLOR_BGR2RGB)
        if original is True:
            return image
        else:
            return cls.preprocess(image, normalize=normalize)

    @classmethod
    def preprocess(cls, original: np.array, normalize: bool = True):
        height, width = cls.shape[1:3]
        resized = cv2.resize(original, dsize=(width, height), interpolation=cv2.INTER_LINEAR)
        hsv = cv2.cvtColor(resized, cv2.COLOR_RGB2HSV)
        s_channel = hsv[:, :, [1]]
        if normalize:
            normalized = s_channel / 127.5 - 1
            return normalized.astype(np.float32)
        else:
            return s_channel.astype(np.uint8)

    @classmethod
    def postprocess(cls, original: np.array, preprocessed: np.array, true_steer_angle: float, pred_steer_angle: float):
        height, width = original.shape[:2]
        steer_strip = np.zeros([10, width, 3], dtype=np.uint8)
        if true_steer_angle is not None:
            true_steer_pixel = int(((true_steer_angle * width) + width) / 2)
            cv2.rectangle(steer_strip, pt1=(true_steer_pixel - 5, 0), pt2=(true_steer_pixel + 5, 10),
                          thickness=cv2.FILLED, color=[0, 255, 0])
        if pred_steer_angle is not None:
            pred_steer_pixel = int(((pred_steer_angle * width) + width) / 2)
            cv2.rectangle(steer_strip, pt1=(pred_steer_pixel - 4, 2), pt2=(pred_steer_pixel + 4, 8),
                          thickness=cv2.FILLED, color=[0, 0, 255])
        preprocessed = cv2.resize(preprocessed, dsize=(width, height), interpolation=cv2.INTER_NEAREST)
        if len(preprocessed.shape) < 3 or preprocessed.shape[2] == 1:
            preprocessed = cv2.cvtColor(preprocessed, code=cv2.COLOR_GRAY2RGB)
        return np.vstack((original, steer_strip, preprocessed))


class TestPreprocessor(unittest.TestCase):
    def test_1_preprocessing(self) -> None:
        preproc = Preprocessor()

        random_file, steer_angle = random.sample(preproc.files.items(), k=1)[0]
        original = Preprocessor.open(random_file, original=True, normalize=False)
        preprocessed = Preprocessor.open(random_file, original=False, normalize=False)
        flipped = cv2.flip(preprocessed, 1)
        plt.figure()
        subplot = plt.subplot(2, 1, 1)
        subplot.set_title("Preprocessing Example - {}".format(random_file))
        subplot.axes.get_xaxis().set_visible(False)
        subplot.axes.get_yaxis().set_visible(False)
        plt.imshow(original)
        subplot = plt.subplot(2, 2, 3)
        subplot.set_title("Steering Angle = {:.1%}".format(steer_angle))
        subplot.axes.get_xaxis().set_visible(False)
        subplot.axes.get_yaxis().set_visible(False)
        plt.imshow(preprocessed.squeeze(), cmap='gray')
        subplot = plt.subplot(2, 2, 4)
        subplot.set_title("Steering Angle = {:.1%}".format(-steer_angle))
        subplot.axes.get_xaxis().set_visible(False)
        subplot.axes.get_yaxis().set_visible(False)
        plt.imshow(flipped.squeeze(), cmap='gray')
        plt.show(block=True)

    def test_2_generator(self) -> None:
        rows, columns = 3, 3
        preproc = Preprocessor(batch_size=rows)

        plt.figure()
        for row, (images, steer_angles) in zip(range(rows), preproc):
            for column, image, steer_angle in zip(range(columns), images, steer_angles):
                idx = row * columns + (column + 1)
                subplot = plt.subplot(rows, columns, idx)
                subplot.set_title("Steering Angle = {:.1%}".format(steer_angle))
                subplot.axes.get_xaxis().set_visible(False)
                subplot.axes.get_yaxis().set_visible(False)
                plt.imshow(image.squeeze(), cmap='gray')
        plt.show(block=False)

        plt.figure()
        plt.title("Histogram of Steering Angles")
        plt.hist(preproc.steer_angles, bins=45, range=[-1, +1], align='mid')
        plt.show(block=True)

    def test_3_postprocessing(self) -> None:
        preproc = Preprocessor()

        plt.figure()
        plt.gca().get_xaxis().set_visible(False)
        plt.gca().axes.get_yaxis().set_visible(False)
        movie = plt.imshow(np.ones((1, 1, 3)))
        for idx, (image_file, steer_angle) in enumerate(preproc.files.items()):
            if not plt.get_fignums():
                break
            plt.title("Preprocessing ({}/{})".format(idx, len(preproc.files)))
            original = Preprocessor.open(image_file, original=True, normalize=False)
            preprocessed = Preprocessor.open(image_file, original=False, normalize=False)
            mixed = Preprocessor.postprocess(original, preprocessed, steer_angle, None)
            movie.set_array(mixed)
            plt.pause(0.01)
