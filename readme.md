# Udacity Nanodegree ND013 - Self-Driving Car Engineer (December 2016)


Lesson videos, quizes, projects and other materials can be viewed at
the [course webpage](https://www.udacity.com/course/self-driving-car-engineer-nanodegree--nd013).


## Prerequisites

Entering students are expected to have prior experience in Python or another scripting language.
You should have at least some background in probability and statistics and calculus prior to
enrolling. If you have no statistics background, we recommend you take our Intro To Statistics
course. You should also learn foundational Python programming concepts before enrolling in this
Nanodegree. Our Programming Foundations With Python course may be a good place to start.


## Course Description

Self-driving cars represent one of the most significant advances in modern history. Their impact
will go beyond technology, beyond transportation, beyond urban planning to change our daily lives
in ways we have yet to imagine.

Students who enroll in this program will master technologies that are going to shape the future.
Through interactive projects in computer vision, robotic controls, localization, path planning,
and more, you’ll prepare yourself for a key role in this incredible field.


## Projects - Term 1

### Lesson 2 - Finding Lanes [`02-lanes/`](02-lanes/)
<img src="02-lanes/lanes.jpg" width="200" alt="Lesson 2" />

### Lesson 7 - MiniFlow [`07-miniflow/`](07-miniflow/)
<img src="07-miniflow/miniflow.jpg" width="200" alt="Lesson 7" />

### Lesson 8 - TensorFlow [`08-tensorflow/`](08-tensorflow/)
<img src="08-tensorflow/tensorflow.jpg" width="200" alt="Lesson 8" />

### Lesson 9 - Deep Neural Networks [`09-dnn/`](09-dnn/)
<img src="09-dnn/dnn.jpg" width="200" alt="Lesson 9" />

### Lesson 10 - Convolutional Neural Networks [`10-cnn/`](10-cnn/)
<img src="10-cnn/cnn.jpg" width="200" alt="Lesson 10" />

### Lesson 11 - Traffic Signs Classifier [`11-traffic_signs/`](11-traffic_signs/)
<img src="11-traffic_signs/signs.jpg" width="200" alt="Lesson 11" />

### Lesson 12 - Keras [`12-keras/`](12-keras/)
<img src="12-keras/keras.jpg" width="200" alt="Lesson 12" />

### Lesson 13 - Transfer Learning [`13-transfer_learning/`](13-transfer_learning/)
<img src="13-transfer_learning/transfer_learning.jpg" width="200" alt="Lesson 13" />

### Lesson 14 - Behavioral Cloning [`14-behavioral_cloning/`](14-behavioral_cloning/)
<img src="14-behavioral_cloning/behavioral_cloning.png" width="200" alt="Lesson 14" />

### Lesson 18 - Advanced Lane Detection [`18-advanced_lanes/`](18-advanced_lanes/)
<img src="18-advanced_lanes/advanced_lanes.png" width="200" alt="Lesson 18" />

### Lesson 20 - Vehicle Tracking [`20-vehicle_tracking/`](20-vehicle_tracking/)
<img src="20-vehicle_tracking/vehicle_tracking.jpg" width="200" alt="Lesson 20" />
